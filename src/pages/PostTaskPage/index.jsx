import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { connect } from 'react-redux';
import useTaskForm from '../../hooks/useTaskForm';
import PostForm from '../../components/TaskForm';
import { PostTask } from './PostTaskPage.style';

const PostTaskPage = ({ values }) => {
  const currentUser = useSelector((state) => state.currentUser);
  const { postTask } = useTaskForm();

  const handleSubmit = (e) => {
    e.preventDefault();
    postTask(currentUser.user.id, values);
  };

  return (
    <PostTask>
      <div className="title">
        <h1>Post a Task</h1>
      </div>
      <PostForm handleSubmit={handleSubmit} />
    </PostTask>
  );
};

PostTaskPage.propTypes = {
  values: PropTypes.object.isRequired,
};

const mapStateToProps = ({ taskForm }) => ({
  values: taskForm,
});

export default connect(mapStateToProps)(PostTaskPage);
