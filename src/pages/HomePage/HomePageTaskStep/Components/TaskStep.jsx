import React from 'react';
import PropTypes from 'prop-types';
import { Box, StepStyle, StepNum, StepDescription } from './TaskStep.style';

const HomePageTaskStep = ({ step, description }) => (
  <Box>
    <StepStyle>
      <StepNum>{step}</StepNum>
    </StepStyle>
    <StepDescription>{description}</StepDescription>
  </Box>
);

HomePageTaskStep.propTypes = {
  step: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
};

export default HomePageTaskStep;
