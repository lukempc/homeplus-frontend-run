import { React, useState, useEffect, useCallback, useMemo } from 'react';
import _ from 'lodash';
import { useSelector, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import TextField from '@mui/material/TextField';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import EventIcon from '@mui/icons-material/Event';
import MapsHomeWorkIcon from '@mui/icons-material/MapsHomeWork';
import HotelIcon from '@mui/icons-material/Hotel';
import ShowerIcon from '@mui/icons-material/Shower';
import EscalatorIcon from '@mui/icons-material/Escalator';
import StairsIcon from '@mui/icons-material/Stairs';
import CategoryIcon from '@mui/icons-material/Category';
import PaidIcon from '@mui/icons-material/Paid';
import FavoriteIcon from '@mui/icons-material/Favorite';
import TaskStatusTag from '../TaskStatusTag';
import InfoCard from '../InformationCard';
import CustomButton from '../CustomButton';
import CommentBox from '../CommentBox';
import OfferCard from '../OfferCard';
import OfferFormModal from '../OfferFormModal';
import useComment from '../../hooks/useComment';
import { DetailCard } from './TaskDetailCard.style';
import useOffer from '../../hooks/useOffer';
import { askLogin, followTask } from '../../store/reducers/user/user.actions';
import useFollow from '../../hooks/useFollow';

const TaskDetailCard = ({ details, taskComments, taskOffers, setTask }) => {
  const currentUser = useSelector((state) => state.currentUser);
  const dispatch = useDispatch();
  const { postFollow } = useFollow();
  const { offersRequest, offers } = useOffer();
  const { postComment, commentsRequest, comments } = useComment();
  const [newComment, setNewComment] = useState();
  const [displayTask, setDisplayTask] = useState(details);
  const [displayOffers, setDisplayOffers] = useState([]);
  const [displayComments, setDisplayComments] = useState([]);
  const [showOfferFormModal, setShowOfferFormModal] = useState(false);
  const [isAssigned, setIsAssigned] = useState(false);
  const [isFollowing, setIsFollowing] = useState(details.following);

  useEffect(() => {
    if (comments) {
      setDisplayComments(comments);
    }
    if (offers) {
      setDisplayOffers(offers);
    }
    if (displayTask !== details) {
      setDisplayTask(details);
      if (!_.isEmpty(currentUser.followedTasks)) {
        setIsFollowing(currentUser.followedTasks.map((follow) => follow.id).includes(details.id));
      } else {
        setIsFollowing(false);
      }
    }
  }, [offers, comments, currentUser, displayTask, details]);

  const handleChange = (e) => {
    e.preventDefault();
    setNewComment(e.target.value);
  };

  const openOfferFormModal = () => {
    setShowOfferFormModal(!showOfferFormModal);
  };

  const presentOffers = useCallback(
    (o) => {
      Object.values(o).forEach((offer) => {
        if (offer.offer_status.includes('ACCEPTED')) {
          setIsAssigned(true);
        }
      });
      return Object.values(o)
        ?.sort((a, b) => new Date(a.created_time) - new Date(b.created_time))
        .filter((offer) => offer.offer_status !== 'CANCELED')
        .filter((offer) => offer.offer_status !== 'REJECTED')
        .map((offer) => (
          <OfferCard
            key={offer.id}
            offer={offer}
            isAssigned={isAssigned}
            setIsAssigned={setIsAssigned}
            setTask={setTask}
            taskDetails={displayTask}
          />
        ));
    },
    [isAssigned, displayTask, setTask]
  );

  const presentComments = useCallback((c) => {
    return Object.values(c)
      ?.sort((a, b) => new Date(b.created_time) - new Date(a.created_time))
      .map((comment) => <CommentBox key={comment.id} comment={comment} />);
  }, []);

  const presentOffersMemo = useMemo(() => {
    if (!!displayOffers.length) {
      return presentOffers(displayOffers);
    } else if (!!taskOffers.length && !displayOffers.length) {
      return presentOffers(taskOffers);
    } else {
      return <h4>No offers yet</h4>;
    }
  }, [displayOffers, taskOffers, presentOffers]);

  const presentCommentsMemo = useMemo(() => {
    if (!!displayComments.length) {
      return presentComments(displayComments);
    } else if (!!taskComments.length && !displayComments.length) {
      return presentComments(taskComments);
    } else {
      return <h4>Leave the first comment</h4>;
    }
  }, [displayComments, taskComments, presentComments]);

  const resetComment = async () => {
    setTimeout(() => {
      setNewComment('');
      commentsRequest(displayTask.id);
    }, 300);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (!currentUser.loggedIn) {
      dispatch(askLogin());
      return;
    }
    postComment({ task_id: displayTask.id, user_id: currentUser.user.id, message: newComment });
    resetComment();
  };

  const handleFollow = () => {
    if (!currentUser.loggedIn) {
      dispatch(askLogin());
      return;
    }
    postFollow({ user_id: currentUser.user.id, task_id: displayTask.id });
    dispatch(followTask(displayTask));
    setIsFollowing(!isFollowing);
  };

  const theme = useMemo(() => {
    switch (displayTask.task_status) {
      case 'open':
        return 'primary';
      case 'assigned':
        return 'secondary';
      case 'completed':
        return 'success';
      case 'canceled':
        return 'disabled';
      default:
        return;
    }
  }, [displayTask]);

  const categoryLayout = useCallback(
    (category) => {
      switch (category) {
        case 'cleaning':
          return (
            <>
              <InfoCard title="Type" content={displayTask.house_type} icon={<MapsHomeWorkIcon />} />
              <InfoCard title="Rooms" content={displayTask.num_of_rooms} icon={<HotelIcon />} />
              <InfoCard title="Bathrooms" content={displayTask.num_of_bathrooms} icon={<ShowerIcon />} />
            </>
          );
        case 'removal':
          return (
            <>
              <InfoCard title="Type" content={displayTask.house_type} icon={<MapsHomeWorkIcon />} />
              <InfoCard title="Levels" content={displayTask.levels} icon={<StairsIcon />} />
              <InfoCard title="Lift" content={displayTask.lift ? 'Yes' : 'No'} icon={<EscalatorIcon />} />
            </>
          );
        case 'handy':
          return (
            <>
              <InfoCard title="Item" content={displayTask.item_name} icon={<CategoryIcon />} />
              <InfoCard title="Value" content={`$${displayTask.item_value}`} icon={<PaidIcon />} />
            </>
          );
        default:
          return;
      }
    },
    [displayTask]
  );

  const address = `${displayTask.postcode ? `${displayTask.postcode}, ${displayTask.state}` : 'Remote mode'}`;

  const timeSection = useMemo(() => {
    if (displayTask.timeSectionsEntity) {
      return displayTask.timeSectionsEntity.description;
    } else {
      return 'All day';
    }
  }, [displayTask]);

  const dateAndTime = useMemo(() => {
    return `${displayTask.date.split('T')[0].replaceAll('-', '/')}, ${timeSection}`;
  }, [displayTask, timeSection]);

  return (
    <>
      <DetailCard>
        <div className="taskStatus">
          <TaskStatusTag label={_.upperFirst(displayTask.task_status)} color={theme} size="large" />
          <TaskStatusTag
            label="Follow"
            icon={<FavoriteIcon sx={{ width: 20, height: 20 }} />}
            color={isFollowing ? 'warning' : 'info'}
            variant={isFollowing ? 'filled' : 'outlined'}
            size="large"
            onClick={handleFollow}
          />
        </div>
        <div className="cardHeader">
          <div className="cardTitle">
            <h2>{_.upperFirst(displayTask.title)}</h2>
          </div>
          <InfoCard title="TASK BUDGET" content={`$${displayTask.budget}`} />
          <CustomButton
            color="primary"
            variant="contained"
            onClick={openOfferFormModal}
            disabled={displayTask.task_status === 'completed'}
          >
            Make an offer
          </CustomButton>
        </div>
        <div className="cardImportInfo">
          <InfoCard
            title="POSTED BY"
            content={displayTask.userEntity.name}
            avatar={displayTask.userEntity.avatar}
            isAvatar
          />
          <InfoCard title="LOCATION" content={address} icon={<LocationOnIcon />} />
          <InfoCard title="DUE DATE & TIME" content={dateAndTime} icon={<EventIcon />} />
        </div>
        <div className="longLine" />
        <div className="cardOtherInfo">
          <div className="taskInfo">
            <div className="description">
              <h3>Details</h3>
              <span>{displayTask.description ? displayTask.description : 'Nan'}</span>
            </div>
            <div className="taskSubInfo">
              <h3>Other info</h3>
              {categoryLayout(displayTask.category)}
            </div>
          </div>
          <div className="offersBox">
            <h3>Offers</h3>
            {presentOffersMemo}
            <br />
            <CustomButton
              color="primary"
              variant="contained"
              onClick={openOfferFormModal}
              disabled={displayTask.task_status === 'completed'}
            >
              Make an offer
            </CustomButton>
          </div>
          <div className="commentArea">
            <form onSubmit={handleSubmit}>
              <div className="commentBox">
                <TextField
                  id="standard-basic"
                  label="comment"
                  variant="standard"
                  value={newComment}
                  sx={{ width: '70%' }}
                  onChange={handleChange}
                />
                <CustomButton color="primary" variant="outlined" size="small" type="submit">
                  Send
                </CustomButton>
              </div>
            </form>
            {presentCommentsMemo}
          </div>
        </div>
      </DetailCard>
      <OfferFormModal
        showModal={showOfferFormModal}
        setShowModal={setShowOfferFormModal}
        offersRequest={offersRequest}
      />
    </>
  );
};

TaskDetailCard.propTypes = {
  details: PropTypes.object.isRequired,
  taskComments: PropTypes.array.isRequired,
  taskOffers: PropTypes.array.isRequired,
  setTask: PropTypes.func.isRequired,
};

export default TaskDetailCard;
