import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';

const PaymentInput = ({ formData, setFormData }) => {
  const boxStyle2 = {
    display: 'flex',
    flexDirection: 'column',
    marginTop: '10px',
  };

  const [isError, setError] = useState(false);

  const handleChange = (e) => {
    e.preventDefault();
    setError(false);
    if (formData.bank_bsb.length < 5) setError(true);
    if (formData.bank_account.length < 7) setError(true);
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  return (
    <Box sx={{ display: 'flex', flexDirection: 'row', margin: '10px' }}>
      <Box sx={{ ...boxStyle2, marginRight: '10px', width: '150px' }}>
        <Typography>BSB</Typography>
        <TextField
          name="bank_bsb"
          type="number"
          value={formData.bank_bsb}
          onChange={handleChange}
          id="outlined-basic"
          size="small"
          sx={{ width: '100px', marginTop: '5px' }}
          error={isError}
        />
      </Box>
      <Box sx={{ ...boxStyle2 }}>
        <Typography>Account Number</Typography>
        <TextField
          name="bank_account"
          type="number"
          value={formData.bank_account}
          onChange={handleChange}
          id="outlined-basic"
          size="small"
          sx={{ width: '450px', marginTop: '5px' }}
          error={isError}
        />
      </Box>
    </Box>
  );
};

PaymentInput.propTypes = {
  formData: PropTypes.object.isRequired,
  setFormData: PropTypes.func.isRequired,
};

export default PaymentInput;
