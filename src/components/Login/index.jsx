import React, { useRef, useEffect, useCallback, useState } from 'react';
import _ from 'lodash';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { useSpring, animated } from 'react-spring';
import PropTypes from 'prop-types';
import TextField from '@mui/material/TextField';
import RegisterModal from '../Register/index.jsx';
import LogoImg from '../../assets/logo.PNG';
import CustomButton from '../CustomButton';
import {
  Wrapper,
  Form,
  Background,
  ModalWrapper,
  ModalContent,
  CloseModalButton,
  Text,
  Container,
  ButtonSet,
  Logo,
} from './Login.style.jsx';
import { logOut, turnOffLogin } from '../../store/reducers/user/user.actions';
import useUser from '../../hooks/useUser';

const LoginModal = ({ showModal, setShowModal }) => {
  const { login, passwordResetMail } = useUser();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const modalRef = useRef();
  const [showRegisterModal, setShowRegisterModal] = useState(false);
  const [isError, setError] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');
  const [isResetPassword, setResetPassword] = useState(false);
  const [resetMessage, setResetMessage] = useState('');
  const initData = {
    email: '',
    password: '',
  };
  const [userInfo, setUserInfo] = useState(initData);

  const handleResetMode = () => {
    setResetPassword(true);
    setUserInfo(initData);
  };

  const openRegisterModal = () => {
    setShowModal(false);
    setShowRegisterModal((prev) => !prev);
  };

  const handleReset = async () => {
    if (_.isEmpty(userInfo.email)) return;
    const res = await passwordResetMail(userInfo.email);
    setResetMessage(res);
    if (res !== 'success') setError(true);
    alert(userInfo.email);
    setUserInfo(initData);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (_.isEmpty(userInfo.email) || _.isEmpty(userInfo.password)) return;

    const res = await login(userInfo);
    if (res.length > 50) {
      localStorage.setItem('AUTH_TOKEN', res);
      dispatch(logOut());
      dispatch(turnOffLogin());
      setShowModal(false);
      navigate('/', { replace: true });
    } else {
      setError(true);
      setErrorMessage(res);
    }
  };

  const handleChange = (e) => {
    e.preventDefault();
    setUserInfo({ ...userInfo, [e.target.name]: e.target.value });
  };

  const animation = useSpring({
    config: {
      duration: 250,
    },
    opacity: showModal ? 1 : 0,
    transform: showModal ? `translateY(0%)` : `translateY(-100%)`,
  });

  const closeModal = (e) => {
    if (modalRef.current === e.target) {
      setShowModal(false);
    }
  };

  const keyPress = useCallback(
    (e) => {
      if (e.key === 'Escape' && showModal) {
        setShowModal(false);
      }
    },
    [setShowModal, showModal]
  );

  useEffect(() => {
    document.addEventListener('keydown', keyPress);
    return () => document.removeEventListener('keydown', keyPress);
  }, [keyPress]);
  //TODO: maybe add use google, facebook to login
  return (
    <>
      {showModal ? (
        <Background onClick={closeModal} ref={modalRef}>
          <animated.div style={animation}>
            <ModalWrapper showModal={showModal}>
              <ModalContent>
                <Wrapper>
                  {!isResetPassword ? (
                    <Form onSubmit={handleSubmit}>
                      <Logo src={LogoImg} />
                      <TextField
                        type="email"
                        name="email"
                        placeholder="Type your email here..."
                        value={userInfo.email}
                        onChange={handleChange}
                        error={isError}
                      />
                      <br />
                      <TextField
                        type="password"
                        name="password"
                        placeholder="Enter your password..."
                        value={userInfo.password}
                        onChange={handleChange}
                        error={isError}
                        helperText={isError ? errorMessage : null}
                        sx={{ mb: 3 }}
                      />
                      <CustomButton variant="contained" size="large" type="submit">
                        Login
                      </CustomButton>
                      <br />
                      <Text>
                        Forget your password click
                        <CustomButton color="black" size="small" onClick={handleResetMode}>
                          Here
                        </CustomButton>
                      </Text>
                      <Text>
                        Don&apos;t have an account click here
                        <CustomButton color="black" size="small" onClick={openRegisterModal}>
                          Here
                        </CustomButton>
                      </Text>
                    </Form>
                  ) : (
                    <Form>
                      <h3>Send reset link to your email</h3>
                      <TextField
                        type="email"
                        name="email"
                        placeholder="Type your email here..."
                        value={userInfo.email}
                        onChange={handleChange}
                        error={isError}
                        helperText={resetMessage}
                      />
                      <br />
                      <ButtonSet>
                        <CustomButton color="primary" variant="outlined" onClick={() => setResetPassword(false)}>
                          back to login
                        </CustomButton>
                        <CustomButton color="primary" variant="contained" onClick={handleReset}>
                          send link
                        </CustomButton>
                      </ButtonSet>
                    </Form>
                  )}
                </Wrapper>
              </ModalContent>
              <CloseModalButton aria-label="Close modal" onClick={() => setShowModal((prev) => !prev)} />
            </ModalWrapper>
          </animated.div>
        </Background>
      ) : null}
      <Container>
        <RegisterModal showModal={showRegisterModal} setShowModal={setShowRegisterModal} />
      </Container>
    </>
  );
};

LoginModal.propTypes = {
  showModal: PropTypes.bool,
  setShowModal: PropTypes.func,
};

export default LoginModal;
