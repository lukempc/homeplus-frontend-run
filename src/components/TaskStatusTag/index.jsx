import * as React from 'react';
import PropTypes from 'prop-types';
import Chip from '@mui/material/Chip';
import Stack from '@mui/material/Stack';
import { createTheme, ThemeProvider } from '@mui/material/styles';

const theme = createTheme({
  palette: {
    primary: {
      main: '#5ec6f2',
      contrastText: '#fff',
    },
    secondary: {
      main: '#4ba6f5',
    },
    success: {
      main: '#166a8f',
    },
    info: {
      main: '#D4D2D5',
      contrastText: '#444',
    },
    warning: {
      main: '#ff9900',
      contrastText: '#fff',
    },
  },
});

const TaskStatusTag = ({ ...props }) => (
  <Stack direction="row" spacing={2}>
    <ThemeProvider theme={theme}>
      <Chip {...props} sx={{ fontSize: '1em', marginRight: '1rem', padding: 0.5, fontWeight: 500 }} />
    </ThemeProvider>
  </Stack>
);

TaskStatusTag.propTypes = {
  props: PropTypes.node,
};

export default TaskStatusTag;
