import React, { useState } from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import axios from 'axios';
import { CardElement, useStripe, useElements } from '@stripe/react-stripe-js';

export default function CheckoutForm({ success, task, setEvents }) {
  const stripe = useStripe();
  const elements = useElements();
  const total_price = _.round(task?.price * 1.1, 1);
  const [message, setMessage] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (!stripe || !elements) return;

    setIsLoading(true);

    const { error, paymentMethod } = await stripe.createPaymentMethod({
      type: 'card',
      card: elements.getElement(CardElement),
    });

    if (!error) {
      const { id } = paymentMethod;

      try {
        const { data } = await axios.post(
          'https://3lpy82sref.execute-api.us-east-1.amazonaws.com/stripe-payment/create-payment-intent',
          {
            id,
            amount: total_price * 100,
          }
        );
        setTimeout(() => {
          setMessage(data);
        }, 5000);
        success();
        setEvents((events) => {
          const modifiedEvents = events.map((event) => {
            return { ...event, resource: { ...event.resource, paid: event.resource.id === task.id } };
          });
          return modifiedEvents;
        });
      } catch (err) {
        setMessage('Unsuccessful payment. An unexpected error occured.');
      }
    }

    setIsLoading(false);
  };

  return (
    <>
      <form id="payment-form" onSubmit={handleSubmit}>
        <CardElement id="payment-element" />
        <button disabled={isLoading || !stripe || !elements} id="submit">
          <span id="button-text">{isLoading ? <div className="spinner" id="spinner"></div> : 'Pay now'}</span>
        </button>
        {message && <div id="payment-message">{message}</div>}
      </form>
    </>
  );
}

CheckoutForm.propTypes = {
  success: PropTypes.func,
  task: PropTypes.object,
  setEvents: PropTypes.func,
};
