import React from 'react';
import PropTypes from 'prop-types';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';

const Introduction = ({ tasker }) => {
  return (
    <Box sx={{ display: 'flex', flexDirection: 'column', margin: '50px', marginTop: 0 }}>
      <Typography variant="h5" gutterBottom component="div" sx={{ fontWeight: 400 }}>
        Introduction
      </Typography>
      <Typography>{tasker.introduction}</Typography>
      <Typography variant="h5" gutterBottom component="div" sx={{ fontWeight: 400, marginTop: '40px' }}>
        Skill Description
      </Typography>
      <Typography>{tasker.skills_description}</Typography>
    </Box>
  );
};

Introduction.propTypes = {
  tasker: PropTypes.object.isRequired,
};

export default Introduction;
