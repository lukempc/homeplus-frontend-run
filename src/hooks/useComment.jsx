import { useState } from 'react';
import AppApi from '../utils/axios';

const UseComment = () => {
  const [isCommentError, setError] = useState(false);
  const [isCommentLoading, setLoading] = useState(false);
  const [comments, setComments] = useState([]);

  const postComment = async (values) => {
     AppApi('post', '/leave-comment', values);
  };

  const getCommentsByTask = async (task_id) => {
    const data = await AppApi('get', `/task-comments?id=${task_id}`);

    if (!data || !data.length) {
      setError('Somthing went wrong');
      setLoading(false);
      return;
    }

    return data;
  };

  const commentsRequest = async (task_id) => {
    setLoading(true);
    setError(false);

    const data = await getCommentsByTask(task_id);

    setLoading(false);
    setComments(data);
  };

  return {
    postComment,
    commentsRequest,
    isCommentError,
    isCommentLoading,
    comments,
  };
};

export default UseComment;
