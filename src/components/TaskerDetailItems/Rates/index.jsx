import React from 'react';
import PropTypes from 'prop-types';
import Box from '@mui/material/Box';
import Rating from '@mui/material/Rating';
import Typography from '@mui/material/Typography';

const Rates = ({ rating }) => {
  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'column',
        alignSelf: 'flex-end',
        width: '250px',
        height: '100px',
      }}
    >
      {rating ? (
        <>
          <Rating name="read-only" value={rating.sumOfRating} precision={0.5} readOnly />
          <Typography
            component="legend"
            sx={{ paddingTop: 2 }}
          >{`${rating.sumOfRating} stars from ${rating.numOfReviews} reviews`}</Typography>
        </>
      ) : (
        <p>No reviews yet</p>
      )}
    </Box>
  );
};

Rates.propTypes = {
  rating: PropTypes.object.isRequired,
};

export default Rates;
