import styled from 'styled-components';

export const Box = styled.div`
  display: flex; 
  flex-direction: column;
  margin-right: 50px;
  a:-webkit-any-link {
    color: #BBC2DC;
    margin-bottom: 10px;
  }
  h3{
    color:#fff;
    margin-top: 50px;
  }
  
`;