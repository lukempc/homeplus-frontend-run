import React, { lazy } from 'react';
import { Route as RouterPage, Routes, Navigate } from 'react-router-dom';

const HomePage = lazy(() => import('../pages/HomePage'));
const ErrorPage = lazy(() => import('../pages/ErrorPage'));
const LoginPage = lazy(() => import('../pages/LoginPage'));
const RegisterPage = lazy(() => import('../pages/RegisterPage'));
const PostTaskPage = lazy(() => import('../pages/PostTaskPage'));
const DashboardPage = lazy(() => import('../pages/DashboardPage'));
const TaskerDetailPage = lazy(() => import('../pages/TaskerDetailPage'));
const BrowsePage = lazy(() => import('../pages/BrowsePage'));
const AccountVerifyPage = lazy(() => import('../pages/AccountVerifyPage'));
const ResetPasswordPage = lazy(() => import('../pages/RestPasswordPage'));

const Route = () => (
  <Routes>
    <RouterPage path="/" element={<Navigate replace to="/home" />} />
    <RouterPage path="/login" element={<LoginPage />} />
    <RouterPage path="/register" element={<RegisterPage />} />
    <RouterPage path="/home" element={<HomePage />} />
    <RouterPage path="/post-task" element={<PostTaskPage />} />
    <RouterPage path="/tasker-detail/:tasker_id/:tasker_name" element={<TaskerDetailPage />} />
    <RouterPage path="/dashboard" element={<DashboardPage />} />
    <RouterPage path="/browse-tasks" element={<BrowsePage />} />
    <RouterPage path="/account-verify/:token" element={<AccountVerifyPage />} />
    <RouterPage path="/password-reset/:token" element={<ResetPasswordPage />} />
    <RouterPage path="/browse-tasks/:task_id/:title" element={<BrowsePage />} />

    {/* <RouterPage element={<ProtectedRouterPage />}>
            <RouterPage
                path="/dashboard"
                element={
                    <MainLayout>
                        <DashboardPage />
                    </MainLayout>
                }
            />
        </RouterPage> */}
    <RouterPage path="*" element={<ErrorPage />} />
  </Routes>
);

export default Route;
