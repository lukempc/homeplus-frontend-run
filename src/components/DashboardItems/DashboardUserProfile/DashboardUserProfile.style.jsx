import styled from 'styled-components';

export const Wrapper = styled.div`
  width: 90%;
  display: flex;
  line-height: 1.6;
  padding: 3%;
`;

export const Left = styled.div`
  width: 90%;
  height: 100%;
  display: flex;
`;

export const PicArea = styled.div`
  height: 50%;
  margin-right: 5rem;
`;

export const Pic = styled.div`
  position: relative;
  width: 150px;
  height: 150px;
  border-radius: 50%;
  left: 50%;
  transform: translateX(-50%);
  background-color: #444;
`;

export const Username = styled.div`
  height: 40px;
  text-align: center;
  margin-top: 2rem;
`;

export const GenderArea = styled.div`
  margin-top: 2rem;
  padding-left: 4rem;
`;

export const LineWrapper = styled.div`
  height: 40px;
  padding-top: 10px;
  display: flex;
`;

export const Labal = styled.div`
  width: 200px;
  text-align: left;
`;

export const Value = styled.div`
  width: 50%;
  text-align: left;
`;

export const Right = styled.div`
  width: 70%;
  height: 100%;
`;

export const TextArea = styled.div`
  height: 120px;
  width: 100%;
`;
