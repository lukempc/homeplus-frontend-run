import styled from 'styled-components';

export const InfoCard = styled.div`
  h3,
  span {
    color: #444;
  }
  .MuiAvatar-root {
    margin-right: 13%;
    position: relative;
    top: 5px;
  }
  svg {
    position: relative;
    left: 14%;
    width: 30px;
    transform: translateX(-50%);
    transform: scale(1.2);
  }
  display: flex;
  align-items: center;

  .info {
    min-width: 100px;
    span {
      font-size: 0.95rem;
      width: 60%;
    }
    .numbers {
      font-size: 1.5rem;
    }
    h3 {
      font-size: 0.9rem;
    }
  }
`;
