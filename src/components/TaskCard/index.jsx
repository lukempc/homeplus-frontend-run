import React, { useMemo } from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import FavoriteIcon from '@mui/icons-material/Favorite';
import TaskStatusTag from '../TaskStatusTag';
import { Card } from './TaskCard.style';

export default function TaskCard({ details, onClick, actived }) {
  const date = useMemo(() => <p>{details.date.split('T')[0].replaceAll('-', '/')}</p>, [details]);

  const address = `${details.postcode ? `${details.postcode}, ${details.state}` : 'Remote mode'}`;

  const theme = useMemo(() => {
    switch (details.task_status) {
      case 'open':
        return 'primary';
      case 'assigned':
        return 'secondary';
      case 'completed':
        return 'success';
      case 'canceled':
        return 'disabled';
      default:
        return;
    }
  }, [details]);

  return (
    <Card onClick={onClick} style={{ borderColor: actived ? '#2B6777' : '#fff' }}>
      <div className="cardContent">
        <h1 className="title">{_.upperFirst(details.title)}</h1>
        <p>${details.budget}</p>
      </div>
      <div>
        <p>{address}</p>
        {date}
      </div>
      <div className="taskStatus">
        <TaskStatusTag label={_.upperFirst(details.task_status)} color={theme} />
        <TaskStatusTag
          label="Follow"
          color={details.following ? 'warning' : 'info'}
          variant={details.following ? 'filled' : 'outlined'}
          icon={<FavoriteIcon sx={{ width: 20, height: 20 }} />}
        />
      </div>
    </Card>
  );
}

TaskCard.propTypes = {
  details: PropTypes.object.isRequired,
  onClick: PropTypes.func,
  actived: PropTypes.bool,
};
