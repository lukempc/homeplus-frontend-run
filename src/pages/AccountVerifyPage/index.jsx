import React, { useCallback, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import useUser from '../../hooks/useUser';
import LoadingPage from '../../components/LoadingPage';
import { Message } from './AccountVerifyPage.style';
import CustomButton from '../../components/CustomButton';

const AccountVerifyPage = () => {
  const { accountVerify, resendVerify } = useUser();
  const { token } = useParams();
  const [message, setMessage] = useState('');
  const [resend, setResend] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const navigate = useNavigate();

  const isVerified = useCallback(async () => {
    const res = await accountVerify(token);
    if (res === 'confirmed') {
      setMessage(`Your email has been verified! \nYou can now log in with your new account`);
      setTimeout(() => {
        navigate('/', { replace: true });
      }, 3000);
    } else {
      setMessage('Failed, resend verify email');
      setResend(true);
    }
  }, [token, accountVerify, navigate]);

  const handleResend = () => {
    resendVerify(token);
    navigate('/', { replace: true });
  };

  useEffect(() => {
    if (isLoading && token) {
      isVerified();
      setIsLoading(false);
    }
  }, [isLoading, isVerified, token]);

  return (
    <>
      {isLoading ? (
        <LoadingPage />
      ) : (
        <>
          <Message>
            {message.split('\n').map((line, index) => (
              <p key={index}>{line}</p>
            ))}
          </Message>
          {resend ? (
            <CustomButton color="primary" variant="contained" onClick={handleResend}>
              Resend verify link
            </CustomButton>
          ) : null}
        </>
      )}
    </>
  );
};

export default AccountVerifyPage;
