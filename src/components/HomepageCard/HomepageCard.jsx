import React from 'react';
import Card from '@mui/material/Card';
import CustomButton from '../CustomButton';
import { CardContent, Logo, ButtonWrapper, Content } from './HomepageCard.style';

const HomepageCard = ({ src, title, children }) => {
  return (
    <Card
      sx={{
        width: '400px',
        height: '320px',
        margin: ' 0 20px 40px',
        borderRadius: '10px',
        boxShadow: 'rgba(0, 0, 0, 0.16) 0px 3px 6px, rgba(0, 0, 0, 0.23) 0px 3px 6px',
      }}
    >
      <CardContent>
        <Logo src={src} />
        <h3>{title}</h3>
        <Content>{children}</Content>
        <ButtonWrapper>
          <CustomButton size="small">Learn More</CustomButton>
        </ButtonWrapper>
      </CardContent>
    </Card>
  );
};

export default HomepageCard;
