import React from 'react';
import Card from '@mui/material/Card';
import { CardContent, Logo } from './CategoriesCard.style';

const CategoriesCard = ({ src, title }) => {
  const styledCard = {
    width: '230px',
    margin: ' 0 10px 10px',
    '@media (max-width: 768px)': {
      width: '100%',
    },
  };
  return (
    <Card sx={styledCard}>
      <CardContent>
        <Logo src={src} />
        <h3>{title}</h3>
      </CardContent>
    </Card>
  );
};

export default CategoriesCard;
