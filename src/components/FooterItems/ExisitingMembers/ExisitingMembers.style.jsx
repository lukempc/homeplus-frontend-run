import styled from 'styled-components';

export const Box = styled.div`
  display: flex;
  flex-direction: column;
  margin-right: 50px;
  a:-webkit-any-link {
    color: #bbc2dc;
    margin-bottom: 10px;
  }
  @media (max-width: 768px) {
  }
`;

export const Title = styled.h3`
  color: #fff;
`;
