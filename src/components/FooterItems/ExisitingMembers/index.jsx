import React from 'react';
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import Collapse from '@mui/material/Collapse';

import Link from '@mui/material/Link';

import { Box, Title } from './ExisitingMembers.style.jsx';

const ExisitingMembers = () => {
  const [open, setOpen] = React.useState(true);

  const handleClick = () => {
    setOpen(!open);
  };

  return (
    <List>
      <ListItemButton onClick={handleClick}>
        <Title>Exisiting Members</Title>
      </ListItemButton>
      <Collapse in={open} timeout="auto" unmountOnExit>
        <Box>
          <Link href="#">Post a task</Link>
          <Link href="#">Browse tasks</Link>
        </Box>
      </Collapse>
    </List>

    // <Box>
    //     <h3>Exisiting Members</h3>
    //     <Link href="#">Post a task</Link>
    //     <Link href="#">Browse tasks</Link>
    //     <Link href="#">Login</Link>
    //     <Link href="#">Support centre</Link>
    //     <Link href="#">Merchandise</Link>
    // </Box>
  );
};
export default ExisitingMembers;
