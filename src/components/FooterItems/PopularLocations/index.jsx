import React from 'react';
import Link from '@mui/material/Link';
import {Box} from './PopularLocations.style.jsx';

const PopularLocations = () => (
  <Box>
      <h3>Popular Locations</h3>
      <Link href="#">Sydney</Link>
      <Link href="#">Melbourne</Link>
      <Link href="#">Brisbane</Link>
      <Link href="#">Perth</Link>
      <Link href="#">Adelaide</Link>
      <Link href="#">Newcastle</Link>
      <Link href="#">Canberra</Link>
  </Box>
);
export default PopularLocations;