import styled from 'styled-components';

export const Card = styled.div`
  width: 550px;
  padding: 3%;
  background-color: #C8D8E4;
  margin-bottom: 1rem;
  border-radius: 10px;
  hr {
      margin-top: 15px;
  }
  color: #444;
`;

export const TitleRow = styled.div`
display: grid;
  grid-template-columns: auto auto auto;
  gap: 10px;
  align-items: center;
`;

export const TaskerInfo = styled.div`
display: flex;
grid-column-start: 1;
grid-column-end: 3;
justify-content: space-between;
padding-right: 25px;
`;

export const TaskerAva = styled.div`

display: flex;
align-items: center;
justify-content: space-between;
> p {
    padding-left: 15px;
}
`;

export const ButtonSet = styled.div`
display: grid;
`;

export const PosterOnly = styled.div`
display: flex;
padding-left: 25px;
align-items: center;
justify-content: space-between;
> p{
    font-size: 1.5rem;
}
`;

export const OfferContent = styled.p`
    padding-left: 5px;
`;