import React from 'react';
import { Box, Container, CardContainer, Wrapper } from './HomePageTaskStep.style';
import Card from '../../../components/CategoriesCard/';
import Cleaning from '../../../assets/cleaning.png';
import Removalists from '../../../assets/sofa.png';
import Handyperson from '../../../assets/mechanic.png';
import Delivery from '../../../assets/delivery-truck.png';
import Gardening from '../../../assets/gardening.png';
import Electricians from '../../../assets/electrician.png';
import Assembly from '../../../assets/industrial-robot.png';
import Painter from '../../../assets/swatches.png';
import TaskStep from './Components/TaskStep';

const HomePageTaskStep = () => (
  <Box>
    <h2>Post your first task in seconds</h2>
    <Container>
      <Wrapper>
        <TaskStep step="1" description="Describe what you need done" />
        <TaskStep step="2" description="Set your budget" />
        <TaskStep step="3" description="Receive quotes & pick the best person" />
      </Wrapper>
    </Container>
    <CardContainer>
      <Card src={Cleaning} title="Cleaning" />
      <Card src={Removalists} title="Removalists" />
      <Card src={Handyperson} title="Handyperson" />
      <Card src={Delivery} title="Delivery" />
      <Card src={Gardening} title="Gardening" />
      <Card src={Electricians} title="Electricians" />
      <Card src={Assembly} title="Assembly" />
      <Card src={Painter} title="Painter" />
    </CardContainer>
  </Box>
);

export default HomePageTaskStep;
