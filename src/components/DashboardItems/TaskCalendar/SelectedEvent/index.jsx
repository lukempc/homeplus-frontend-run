import React, { useMemo, useState } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import Modal from '@mui/material/Modal';
import CustomButton from '../../../CustomButton';
import { EventBox } from './SelectedEvent.style';
import useTask from '../../../../hooks/useTaskForm';
import SuccessErrorModal from '../../../SuccessErrorModal';
import PaymentModal from '../../StripePayment';

const SelectedEvent = ({ showModal, setShowModal, event, events, setEvents }) => {
  const { cancelTaskByTasker } = useTask();
  const [response, setResponse] = useState('');
  const [openSuccessErrorModal, setSuccessErrorModal] = useState(false);
  const [openPaymentModal, setPaymentModal] = useState(false);
  const [isSuccess, setSuccess] = useState(false);
  const handleClose = () => setShowModal(false);

  const address = useMemo(() => {
    if (!_.isEmpty(event)) {
      return `${event.resource.street}, ${event.resource.suburb}, ${event.resource.state}, ${event.resource.postcode}`;
    } else {
      return '';
    }
  }, [event]);

  const handleComplete = async () => {
    handleClose();
    setPaymentModal(true);
  };

  const handleCancel = async () => {
    const res = await cancelTaskByTasker(event.resource.id);
    SuccessErrorPopout(res === 'success', res);
    handleClose();
    setEvents(events.filter((e) => e !== event));
  };

  const SuccessErrorPopout = (isPosted, res) => {
    if (isPosted) {
      setResponse('Task updated successfully');
    } else {
      setResponse(res);
    }
    setSuccess(isPosted);
    setSuccessErrorModal(true);
  };

  return (
    <>
      <Modal open={showModal} onClose={handleClose}>
        <EventBox>
          <h2>{_.upperFirst(event.title)}</h2>
          <h4>{address}</h4>
          <div>
            <CustomButton variant="outlined" onClick={handleClose}>
              Close
            </CustomButton>
            {event.isMine ? (
              <CustomButton
                variant="contained"
                onClick={handleComplete}
                disabled={event.resource.task_status === 'completed' || event.resource.task_status === 'open'}
              >
                Complete
              </CustomButton>
            ) : (
              <CustomButton variant="contained" color="error" onClick={handleCancel}>
                Cancel task
              </CustomButton>
            )}
          </div>
        </EventBox>
      </Modal>
      <SuccessErrorModal
        isSuccess={isSuccess}
        setShowModal={setSuccessErrorModal}
        showModal={openSuccessErrorModal}
        response={response}
      />
      <PaymentModal
        setShowModal={setPaymentModal}
        showModal={openPaymentModal}
        task={event.resource}
        setEvents={setEvents}
      />
    </>
  );
};

SelectedEvent.propTypes = {
  setShowModal: PropTypes.func.isRequired,
  showModal: PropTypes.bool.isRequired,
  event: PropTypes.object.isRequired,
  events: PropTypes.array.isRequired,
  setEvents: PropTypes.func.isRequired,
};

export default SelectedEvent;
