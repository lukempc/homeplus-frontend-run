import React from 'react';
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';

const LoadingPage = () => (
  // <section className="loadingPage">
  <Box
    sx={{
      display: 'flex',
    }}
  >
    <CircularProgress />
  </Box>
  // </section>
);

export default LoadingPage;
