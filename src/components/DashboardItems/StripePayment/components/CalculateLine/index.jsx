import React from 'react';
import PropTypes from 'prop-types';
import { Line } from './CalculateLine.style';

const CalculateLine = ({ title, money }) => {
  return (
    <Line>
      <div>
        <h4>{title}</h4>
      </div>
      <div>
        <h4>A${money}</h4>
      </div>
    </Line>
  );
};

CalculateLine.propTypes = {
  title: PropTypes.string,
  money: PropTypes.number,
};

export default CalculateLine;
