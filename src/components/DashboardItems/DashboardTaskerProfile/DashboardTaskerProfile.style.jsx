import styled from 'styled-components';

export const Wrapper = styled.div`
  width: 80%;
  display: flex;
  flex-direction: column;
  line-height: 1.6;
`;

export const LineWrapper = styled.div`
  height: 40px;
  width: 100%;
  padding-top: 10px;
  display: flex;
  text-align: left;
`;

export const Label = styled.div`
  width: 200px;
  font-size: 20px;
`;

export const CategoryLabel = styled(Label)`
  padding-top: 15px;
`;

export const TextArea = styled.div`
  padding: 2%;
  min-height: 1rem;
  width: 100%;
  font-size: 17px;
`;
