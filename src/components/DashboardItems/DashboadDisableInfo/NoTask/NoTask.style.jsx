import styled from 'styled-components';

export const Box = styled.div`
  display: flex;
  width: 300px;
  flex-direction: column;
  align-items: center;
  position: relative;
  top: 100px;
`;
