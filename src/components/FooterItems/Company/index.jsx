import React from 'react';
import Link from '@mui/material/Link';
import {Box} from './Company.style.jsx';

const Company = () => (
  <Box>
      <h3>Company</h3>
      <Link href="#">About us</Link>
      <Link href="#">Careers</Link>
      <Link href="#">Media enquiries</Link>
      <Link href="#">Community guidelines</Link>
      <Link href="#">Tasker principles</Link>
      <Link href="#">Terms & conditions</Link>
      <Link href="#">Blog</Link>
      <Link href="#">Contact us</Link>
      <Link href="#">Privacy policy</Link>
      <Link href="#">Investors</Link>
  </Box>
);
export default Company;