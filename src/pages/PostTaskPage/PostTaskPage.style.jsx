import styled from 'styled-components';

export const PostTask = styled.div`
  height: 93vh;
  background-color: white;
  overflow: hidden;
  position: relative;
  left: 50%;
  transform: translateX(-50%);
  .title {
    h1 {
      margin-top: 45px;
    }
    text-align: center;
    height: 15px;
  }
  @media (max-width: 768px) {
    overflow: scroll;
    overflow-x: hidden;
  }
`;
