import { React, useCallback, useEffect, useState } from 'react';
import _ from 'lodash';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { Calendar, momentLocalizer } from 'react-big-calendar';
import moment from 'moment';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import SelectedEvent from './SelectedEvent';

const localizer = momentLocalizer(moment);

const TaskCalendar = ({ tasksOfUser, tasksOfTasker }) => {
  const [events, setEvents] = useState([]);
  const [selected, setSelected] = useState({});
  const [showEvent, setShowEvent] = useState(false);
  const [userTasks, setUserTasks] = useState(null);
  const [taskerTasks, setTaskerTasks] = useState(null);
  const { user } = useSelector((state) => state.currentUser);

  const handleSelected = (event) => {
    setSelected(event);
    setShowEvent(true);
  };

  const eventPropGetter = useCallback((event) => {
    let newStyle = {
      backgroundColor: '#166a8f',
      color: '#fff',
      borderRadius: '0px',
      border: 'none',
    };

    if (event.isMine) {
      newStyle.backgroundColor = '#0d9ddb';
      newStyle.color = '#fff';
    }

    return {
      className: '',
      style: newStyle,
    };
  }, []);

  const event = useCallback(
    (task) => {
      let taskEvent = {
        title: _.upperFirst(task.title),
        start: new Date(task.date),
        end: new Date(task.date),
        resource: task,
      };
      if (task.userEntity.id === user.id) {
        taskEvent = { ...taskEvent, isMine: true };
      }

      return taskEvent;
    },
    [user]
  );

  useEffect(() => {
    if (tasksOfUser && tasksOfUser !== userTasks) {
      setUserTasks(tasksOfUser);
      const totalEvents = Object.values(tasksOfUser)?.map((task) => event(task));
      setEvents(totalEvents);
    }
  }, [tasksOfUser, event, events, userTasks]);

  useEffect(() => {
    if (tasksOfTasker && tasksOfTasker !== taskerTasks) {
      setTaskerTasks(tasksOfTasker);
      const totalEvents = Object.values(tasksOfTasker)?.map((task) => event(task));
      setEvents(totalEvents);
    }
  }, [tasksOfTasker, taskerTasks, event, events]);

  return (
    <div>
      <Calendar
        localizer={localizer}
        events={events}
        selected={selected}
        onSelectEvent={handleSelected}
        startAccessor="start"
        endAccessor="end"
        eventPropGetter={eventPropGetter}
        style={{ height: '75vh', width: '90%' }}
      />
      <SelectedEvent
        showModal={showEvent}
        setShowModal={setShowEvent}
        event={selected}
        events={events}
        setEvents={setEvents}
      />
    </div>
  );
};

TaskCalendar.propTypes = {
  tasksOfUser: PropTypes.any,
  tasksOfTasker: PropTypes.any,
};

export default TaskCalendar;
