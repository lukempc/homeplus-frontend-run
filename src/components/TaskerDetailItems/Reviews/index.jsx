import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import ReviewCard from './ReviewCard';

const TabPanel = ({ children, value, index, ...other }) => {
  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box
          sx={{
            marginTop: '20px',
            display: 'flex',
            flexWrap: 'wrap',
            alignItems: 'center',
            gap: 3,
          }}
        >
          {children}
        </Box>
      )}
    </div>
  );
};

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const Reviews = ({ reviews }) => {
  const [value, setValue] = React.useState(0);
  const [sortType, setSortType] = useState(1);

  const handleSortTypeChange = (event) => {
    setSortType(event.target.value);
  };
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <Box sx={{ display: 'flex', flexDirection: 'column', width: '90%', marginBottom: '20px' }}>
      <Typography variant="h5" gutterBottom component="div" sx={{ fontWeight: 400 }}>
        Reviews
      </Typography>
      <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
        <Tabs value={value} onChange={handleChange} aria-label="basic tabs example">
          <Tab label="As a Tasker" {...a11yProps(0)} />
          <Tab label="As a Poster" {...a11yProps(1)} />
        </Tabs>
      </Box>
      <FormControl sx={{ m: 1, width: '180px', position: 'relative' }}>
        <Select id="sort-select" size="small" value={sortType} onChange={handleSortTypeChange}>
          <MenuItem value={1}>Most Recent</MenuItem>
          <MenuItem value={2}>Most Favourable</MenuItem>
          <MenuItem value={3}>Most Cricital</MenuItem>
        </Select>
      </FormControl>
      <TabPanel value={value} index={0}>
        {!!reviews.length ? (
          reviews.map((review) => <ReviewCard key={review.id} review={review} />)
        ) : (
          <p>This user has no reviews as a Job Tasker yet</p>
        )}
      </TabPanel>
      <TabPanel value={value} index={1}>
        <p>This user has no reviews as a Job Poster yet</p>
      </TabPanel>
    </Box>
  );
};

Reviews.propTypes = {
  reviews: PropTypes.array.isRequired,
};

export default Reviews;
