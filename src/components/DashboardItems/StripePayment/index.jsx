import React, { useState } from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import Modal from '@mui/material/Modal';
import { loadStripe } from '@stripe/stripe-js';
import { Elements } from '@stripe/react-stripe-js';
import CheckoutForm from './components/CheckoutForm';
import { StripeBox, CalculateTable } from './StripePayment.style';
import TaskCompleteModal from '../../TaskCompleteModal';
import CalculateLine from './components/CalculateLine';
import SuccessErrorModal from '../../SuccessErrorModal';
import useTask from '../../../hooks/useTaskForm';

const stripePromise = loadStripe(
  'pk_test_51Kg7FSHhD5Q81mjd66cwGalW5kXa2m6254A2OdgHutCFnUJ31HD2LMfaYbaaktn6y6tNWy35cTMxNZ78eyozH2qn00Gwr9uPLB',
  { locale: 'en' }
);

const StripePayment = ({ showModal, setShowModal, task, setEvents }) => {
  const { putTask } = useTask();
  const [openTaskCompleteModal, setTaskCompleteModal] = useState(false);
  const [response, setResponse] = useState('');
  const [openSuccessErrorModal, setSuccessErrorModal] = useState(false);
  const [isSuccess, setSuccess] = useState(false);
  const handleClose = () => setShowModal(false);

  const handleSuccess = async () => {
    const completedTask = { ...task, task_status: 'completed' };
    const res = await putTask(completedTask);
    handleClose();
    if (res !== 'success') {
      SuccessErrorPopout(false, res);
    } else {
      setTaskCompleteModal(true);
    }
  };

  const SuccessErrorPopout = (isPosted, res) => {
    if (isPosted) {
      setResponse('Task completed successfully');
    } else {
      setResponse(res);
    }
    setSuccess(isPosted);
    setSuccessErrorModal(true);
  };

  const appearance = {
    theme: 'stripe',
  };
  const options = {
    appearance,
  };

  const feeTable = [
    {
      title: 'Subtotal',
      aud: task?.price,
    },
    {
      title: 'Service',
      aud: _.round(task?.price * 0.1, 1),
    },
    {
      title: 'Total',
      aud: _.round(task?.price * 1.1, 1),
    },
  ];

  return (
    <>
      <Modal open={showModal} onClose={handleClose}>
        <StripeBox>
          <h3>Pay for task</h3>
          <CalculateTable>
            {feeTable.map((line, index) => {
              return <CalculateLine key={index} title={line.title} money={line.aud} />;
            })}
          </CalculateTable>
          <Elements options={options} stripe={stripePromise}>
            <CheckoutForm success={handleSuccess} task={task} setEvents={setEvents} />
          </Elements>
        </StripeBox>
      </Modal>
      <TaskCompleteModal setShowModal={setTaskCompleteModal} showModal={openTaskCompleteModal} task={task} />
      <SuccessErrorModal
        isSuccess={isSuccess}
        setShowModal={setSuccessErrorModal}
        showModal={openSuccessErrorModal}
        response={response}
      />
    </>
  );
};

StripePayment.propTypes = {
  showModal: PropTypes.bool,
  setShowModal: PropTypes.func,
  task: PropTypes.object,
  setEvents: PropTypes.func,
};

export default StripePayment;
