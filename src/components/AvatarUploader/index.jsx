import React, { useCallback, useState } from 'react';
import Axios from 'axios';
import { useSelector, useDispatch } from 'react-redux';
import Avatar from '@mui/material/Avatar';
import Tooltip from '@mui/material/Tooltip';
import stringAvatar from '../../utils/avatar.util';
import AvatarCroper from '../AvatarCroper';
import useUser from '../../hooks/useUser';
import { setUser } from '../../store/reducers/user/user.actions';
import Notification from '../DashboardItems/DashboardNotification';
import LoadingPage from '../LoadingPage';

const AvatarApi = Axios.create({
  baseURL: 'https://nnyhqa6jj0.execute-api.us-east-1.amazonaws.com/prod/',
});

const AvatarUploader = () => {
  const { user } = useSelector((state) => state.currentUser);
  const dispatch = useDispatch();
  const { updateUser } = useUser();
  const [progress, setProgress] = useState('getUploaded');
  const [isUploading, setIsuploading] = useState(false);
  const [imageUrl, setImageUrl] = useState(null);
  const [selectedImage, setSelectedImage] = useState(null);
  const [croperShow, setCroperShow] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');
  const avatar = { ...stringAvatar(user.name).sx };
  const sx = { ...avatar, width: 120, height: 120, marginTop: 1 };

  const fileSelectedHandler = async (e) => {
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      reader.readAsDataURL(e.target.files[0]);
      reader.addEventListener('load', () => {
        setSelectedImage(reader.result);
      });
      e.target.value = '';
      setCroperShow(true);
    }
  };

  const fileUploadHandler = async (croppedAvatar) => {
    setProgress('');
    setCroperShow(false);
    setIsuploading(true);
    setErrorMessage('');
    try {
      const { data } = await AvatarApi.post('/', { image: croppedAvatar });
      setImageUrl(data);
      dispatch(setUser({ ...user, avatar: data }));
      updateUser({ ...user, avatar: data });
      setProgress('uploaded');
      setIsuploading(false);
    } catch (error) {
      //console.log('error in upload', error);
      setErrorMessage(error.message);
      setProgress('uploadError');
    }
  };

  const content = () => {
    switch (progress) {
      case 'uploaded':
        return <Notification severity="info">Avatar updated!</Notification>;
      case 'uploadError':
        return <Notification severity="error">{errorMessage}</Notification>;
      default:
        return;
    }
  };

  const avatarStatus = useCallback(() => {
    if (imageUrl) return imageUrl;
    if (user.avatar && !imageUrl) return user.avatar;
    if (!user.avatar && !imageUrl) return null;
  }, [imageUrl, user]);

  return (
    <>
      {!isUploading ? (
        <label htmlFor="uploadAvatar">
          <Tooltip title="Edit">
            <Avatar sx={sx} alt={user.name[0]} src={avatarStatus()} style={{ cursor: 'pointer' }} />
          </Tooltip>
          <input
            type="file"
            onChange={fileSelectedHandler}
            id="uploadAvatar"
            accept="image/*"
            style={{ display: 'none' }}
          />
        </label>
      ) : (
        <LoadingPage />
      )}
      {selectedImage ? (
        <AvatarCroper
          selectedImage={selectedImage}
          setSelectedImage={setSelectedImage}
          fileUploadHandler={fileUploadHandler}
          showModal={croperShow}
          setShowModal={setCroperShow}
        />
      ) : null}
      {content()}
    </>
  );
};

export default AvatarUploader;
