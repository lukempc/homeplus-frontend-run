import { useState } from 'react';
import AppApi from '../utils/axios';

const UseTaskForm = () => {
  const [isError, setError] = useState(false);
  const [isLoading, setLoading] = useState(false);
  const [taskById, setTask] = useState(null);

  const postTask = (user_id, values) => {
    AppApi('post', '/post-task', { user_id, ...values });
  };

  const getTaskById = async (task_id) => {
    setLoading(true);
    setError(false);
    if (task_id !== null) {
      const data = await AppApi('get', `/task?id=${task_id}`);

      if (!data || data.length === 0) {
        setError('Somthing went wrong');
        setLoading(false);
        return;
      }
      setLoading(false);
      setTask(data);
    } else {
      setLoading(false);
      setTask(null);
    }
  };

  const getTasksByUser = async (user_id, keyword) => {
    const data = await AppApi('get', `/task-ukwd?id=${user_id}&keyword=${keyword}`);

    return data;
  };

  const submitUserTaskSearch = async (user_id, keyword) => {
    setLoading(true);
    setError(false);

    const data = await getTasksByUser(user_id, keyword);

    setLoading(false);
    setTasksByUser(data);
  };

  const [tasksByUser, setTasksByUser] = useState(null);

  const getTasksByTasker = async (tasker_id, keyword) => {
    const data = await AppApi('get', `/task-tkwd?id=${tasker_id}&keyword=${keyword}`);

    return data;
  };

  const submitTaskerTasksSearch = async (tasker_id, keyword) => {
    setLoading(true);
    setError(false);

    const data = await getTasksByTasker(tasker_id, keyword);

    setLoading(false);
    setTasksByTasker(data);
  };

  const [tasksByTasker, setTasksByTasker] = useState(null);

  const searchTasksByConditions = async (address, meters, keyword) => {
    const data = await AppApi('get', `/search-tasks?address=${address}&meters=${meters}&keyword=${keyword}`);

    if (!data || !data.length) {
      setError('Somthing went wrong');
      setLoading(false);
      return;
    }

    return data;
  };

  const submitRequest = async (address, meters, keyword) => {
    setLoading(true);
    setError(false);

    const data = await searchTasksByConditions(address, meters, keyword);

    setLoading(false);
    setTasks(data);
  };

  const [tasks, setTasks] = useState(async () => {
    await submitRequest(' ', 0, ' ');
  });

  const putTask = async (task) => {
    const res = await AppApi('put', `/update-task`, task);
    return res;
  };

  const cancelTaskByTasker = async (task_id) => {
    const res = await AppApi('put', `/cancel-task?id=${task_id}`);
    return res;
  };

  return {
    postTask,
    submitRequest,
    getTaskById,
    submitUserTaskSearch,
    submitTaskerTasksSearch,
    cancelTaskByTasker,
    putTask,
    isError,
    isLoading,
    tasks,
    taskById,
    tasksByUser,
    tasksByTasker,
  };
};

export default UseTaskForm;
