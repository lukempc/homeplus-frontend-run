import axios from 'axios';

let baseURL = "https://luke.dreamhomeplus.com/api/v1";
alert(baseURL);
//if (process.env.NODE_ENV === 'production') {
  //baseURL = process.env.REACT_APP_AWS_URL;
//} else if (process.env.NODE_ENV === 'development') {
 // baseURL = process.env.REACT_APP_API_BASE_URL;
//} else {
 // baseURL = process.env.REACT_APP_AWS_URL;
//} 

const api = axios.create({
  baseURL: baseURL,
});

const AppApi = async (method, path, object) => {
  switch (method) {
    case 'post':
      return api
        .post(path, object)
        .then((res) => {
          return res.data;
        })
        .catch((error) => {
          if (error.response) {
            return error.response.data.details[0];
          }
        });
    case 'get':
      return api
        .get(path, object)
        .then((res) => {
          return res.data;
        })
        .catch((error) => {
          if (error.response) {
            return {
              status: error.response.status,
              data: error.response.data
            }
          } 
        });
    case 'put':
      return api
          .put(path, object)
          .then((res) => {
            return res.data;
          })
          .catch((error) => {
            if (error.response) {
              return {
                status: error.response.status,
                data: error.response.data
              }
            } 
          });
    default:
      return;
  }
};

export default AppApi;
