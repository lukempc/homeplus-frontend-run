import * as React from 'react';
import { Box } from './NoTask.style';
import pic from '../../../../assets/EmptyBox.jpg';

const Disable = () => {
  return (
      <Box>
          <img src={pic} alt="Logo" />
          <p> You do not have a task yet!</p>
      </Box>
  );
};

export default Disable;
