import React from 'react';
import Link from '@mui/material/Link';
import { Box } from './PopularCategories.style.jsx';

const PopularCategories = () => (
  <Box>
    <h3>Popular Categories</h3>
    <Link href="#">Cleaning Services</Link>
    <Link href="#">Removalists</Link>
    <Link href="#">Handyperson</Link>
    <Link href="#">Delivery Services</Link>
    <Link href="#">Gardening Services</Link>
    <Link href="#">Auto Electricians</Link>
    <Link href="#">All Services</Link>
  </Box>
);
export default PopularCategories;
