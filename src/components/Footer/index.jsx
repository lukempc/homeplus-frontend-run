import React from 'react';
import { Box, Container } from './Footer.style.jsx';
// import Discover from '../FooterItems/Discover';
// import Company from '../FooterItems/Company';
// import PopularLocations from '../FooterItems/PopularLocations';
// import PopularCategories from '../FooterItems/PopularCategories';
import ExisitingMembers from '../FooterItems/ExisitingMembers';
import CopyRight from '../FooterItems/CopyRight';

const Footer = () => (
  <Box>
    <Container>
      {/* <Discover />
      <Company /> */}
      <ExisitingMembers />
      {/* <PopularCategories />
      <PopularLocations /> */}
    </Container>
    <CopyRight />
  </Box>
);
export default Footer;
