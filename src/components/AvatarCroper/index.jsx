import React, { useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import Cropper from 'react-easy-crop';
import Slider from '@mui/material/Slider';
import Modal from '@mui/material/Modal';
import Button from '../CustomButton';
import { getCroppedImg } from './util/CropImage';
import { ContainerCroper } from './AvatarCroper.style';

const AvatarCroper = ({ selectedImage, showModal, setShowModal, fileUploadHandler }) => {
  const [crop, setCrop] = useState({ x: 0, y: 0 });
  const [zoom, setZoom] = useState(1);
  const [croppedArea, setCroppedArea] = useState(null);

  const handleClose = () => setShowModal(false);

  const UploadCroppedImage = useCallback(async () => {
    try {
      const croppedImage = await getCroppedImg(selectedImage, croppedArea);
      fileUploadHandler(croppedImage);
    } catch (e) {
      //console.error(e);
    }
  }, [croppedArea, selectedImage, fileUploadHandler]);

  const onCropComplete = (creppedAreaPercentage, croppedAreaPixels) => {
    setCroppedArea(croppedAreaPixels);
  };

  return (
    <Modal open={showModal} onClose={handleClose}>
      <ContainerCroper>
        <div className="croper">
          <Cropper
            image={selectedImage}
            crop={crop}
            zoom={zoom}
            aspect={1}
            onCropChange={setCrop}
            onZoomChange={setZoom}
            onCropComplete={onCropComplete}
            style={{ zIndex: '99' }}
          />
        </div>
        <div className="control">
          <div className="slider">
            <Slider min={1} max={3} step={0.1} value={zoom} onChange={(e, z) => setZoom(z)} valueLabelDisplay="on" />
            <span className="zoomLabel">Zoom</span>
          </div>
          <div className="buttons">
            <Button variant="contained" color="error" size="large" onClick={handleClose}>
              Cancel
            </Button>
            <Button variant="contained" size="large" onClick={UploadCroppedImage}>
              Crop
            </Button>
          </div>
        </div>
      </ContainerCroper>
    </Modal>
  );
};

AvatarCroper.propTypes = {
  selectedImage: PropTypes.string.isRequired,
  showModal: PropTypes.bool.isRequired,
  setShowModal: PropTypes.func.isRequired,
  fileUploadHandler: PropTypes.func.isRequired,
};

export default AvatarCroper;
