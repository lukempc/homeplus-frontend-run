import styled from 'styled-components';

export const EventBox = styled.div`
  position: absolute;
  padding: 2%;
  display: flex;
  flex-direction: column;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 300px;
  height: 220px;
  border-radius: 10px;
  box-shadow: 24;
  background-color: #fff;
  align-items: center;
  h2,
  h4 {
    text-align: center;
    line-height: 1.6;
  }
  h2 {
    margin-bottom: 0.5rem;
  }
`;
