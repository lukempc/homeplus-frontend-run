import styled from 'styled-components';

export const CustomForm = styled.form`
  > div {
    position: relative;
    left: 50%;
    transform: translateX(-50%);
  }
  font-family: Roboto, sans-serif;
  .MuiFormControlLabel-root {
    margin-left: 0px;
    margin-right: 12px;
  }
  .MuiFormGroup-root {
    flex-direction: row;
    margin-top: 15;
  }
  h4 {
    font-weight: 400;
  }
  .MuiBox-root {
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 1.2em;
  }
  .MuiStepIcon-root.Mui-active,
  .MuiStepIcon-root.Mui-completed {
    color: #0d9ddb;
  }
  .MuiButton-root.MuiButton-contained {
    background-color: #0d9ddb;
  }
  .MuiButton-root.MuiButton-text,
  .MuiCheckbox-root.Mui-checked {
    color: #0d9ddb;
  }
  .MuiButton-root.Mui-disabled {
    color: rgba(0, 0, 0, 0.26);
  }
  .MuiToggleButton-root.Mui-selected {
    background-color: #166a8f;
    color: #fff;
  }
  .MuiToggleButton-root.Mui-selected:hover {
    background-color: #166a8f;
    color: #fff;
  }
  .MuiStepLabel-root {
    .MuiStepLabel-label {
      font-size: 1.1em;
    }
    svg {
      width: 45px;
      height: 45px;
    }
    circle: {
      cx: 12;
      cy: 12;
      r: 12;
    }
    text: {
      font-size: 0.5em;
    }
  }

  @media (max-width: 768px) {
    > div {
      left: 3%;
    }
    position: relative;
    left: 5px;
    .MuiTypography-h5 {
      font-size: 1.2rem;
    }
    .MuiToggleButtonGroup-root {
      width: 300px;
    }
    .MuiOutlinedInput-root {
      width: 300px;
    }
    .MuiBox-root {
      display: inline;
      font-size: 1em;
      -webkit-justify-content: none;
      justify-content: none;
    }
    .MuiDialog-container.MuiDialog-scrollPaper {
      height: 30%;
      display: inline;
    }
    .MuiModal-root.MuiDialog-root,
    .MuiBackdrop-root {
      max-width: 380px;
    }
    .MuiFormControlLabel-root {
      top: 5px;
    }
    .MuiStepLabel-root {
      .MuiStepLabel-label {
        font-size: 1em;
      }
      svg {
        width: 30px;
        height: 30px;
      }
      circle {
        cx: 12;
        cy: 12;
        r: 12;
      }
    }
  }
`;
