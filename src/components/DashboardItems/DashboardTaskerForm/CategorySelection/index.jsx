import { React } from 'react';
import PropTypes from 'prop-types';
import Box from '@mui/material/Box';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import Typography from '@mui/material/Typography';

const CategorySelection = ({ formData, setFormData }) => {
  const menuItems = ['Cleaning', 'Removal', 'Handyperson'];
  const handleChange = (e) => {
    e.preventDefault();
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };
  return (
    <Box sx={{ display: 'flex', flexDirection: 'row', margin: '10px' }}>
      <Typography sx={{ display: 'flex', marginRight: '10px', width: '150px' }}>Category</Typography>
      <Select
        labelId="category"
        name="category"
        value={formData.category}
        label="Category"
        size="small"
        sx={{ width: '450px' }}
        onChange={handleChange}
      >
        {menuItems.map((item, index) => (
          <MenuItem value={item} key={index}>
            {item}
          </MenuItem>
        ))}
      </Select>
    </Box>
  );
};

CategorySelection.propTypes = {
  formData: PropTypes.object.isRequired,
  setFormData: PropTypes.func.isRequired,
};

export default CategorySelection;
