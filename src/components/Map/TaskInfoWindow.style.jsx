import styled from 'styled-components';

export const TaskInfoWindow = styled.div`
  width: 300px;
  padding: 2%;
  h3,
  p {
    font-weight: 400;
  }
  > div {
    display: flex;
    align-items: center;
    justify-content: space-between;
    .budgetBox {
      width: 80px;
      height: 80px;
      background-color: #f2f2f2;
      border-radius: 3px;
      p {
        text-align: center;
        font-size: 0.9rem;
      }
    }
  }
`;

export const clusterStyle = [
  {
    textColor: 'white',
    textSize: '20px',
    height: 53,
    url: 'https://marker.nanoka.fr/map_cluster-0d9ddb-50.svg',
    width: 53,
  },
  {
    textColor: 'white',
    height: 56,
    url: 'https://marker.nanoka.fr/map_cluster-52AB98-60.svg',
    width: 56,
  },
  {
    textColor: 'white',
    height: 66,
    url: 'https://marker.nanoka.fr/map_cluster-EFBB24-70.svg',
    width: 66,
  },
  {
    textColor: 'white',
    height: 78,
    url: 'https://marker.nanoka.fr/map_cluster-FF0000-80.svg',
    width: 78,
  },
  {
    textColor: 'white',
    height: 90,
    url: 'https://marker.nanoka.fr/map_cluster-6F3381-90.svg',
    width: 90,
  },
];
