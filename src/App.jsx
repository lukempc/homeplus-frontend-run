import React, { Suspense, useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import LoadingPage from './components/LoadingPage';
import Header from './components/Navbar';
import Route from './routes';
import { setUser, logOut, setTasker, setFollowTasks } from './store/reducers/user/user.actions';
import { Container } from './App.style';
import useUser from './hooks/useUser';
import useTasker from './hooks/useTasker';
import useFollow from './hooks/useFollow';

const App = () => {
  const { getUserByJwtToken, isJwtTokenExpired } = useUser();
  const { requestTasker, tasker } = useTasker();
  const { getUserFollows, follows } = useFollow();
  const [currentTasker, setCurrentTasker] = useState();
  const [currentFollows, setCurrentFollows] = useState();
  const [isTaskerData, setIsTaskerData] = useState(false);
  const authToken = localStorage.getItem('AUTH_TOKEN');
  const dispatch = useDispatch();
  const currentUser = useSelector((state) => state.currentUser);

  useEffect(() => {
    if (authToken && !currentUser.loggedIn) {
      getUserByJwtToken(authToken)
        .then((res) => {
          dispatch(setUser(res));
          getUserFollows(res.id);
          if (res.is_tasker_data) {
            setIsTaskerData(true);
            requestTasker(res.id);
          }
        })
        .catch(() => localStorage.removeItem('AUTH_TOKEN'));
    }
    if (!authToken && currentUser.loggedIn) {
      dispatch(logOut());
    }
    if (tasker && currentTasker !== tasker) {
      setCurrentTasker(tasker);
      dispatch(setTasker(tasker));
    }
    if (follows && currentFollows !== follows) {
      setCurrentFollows(follows);
      dispatch(setFollowTasks(follows));
    }
    if (currentUser.loggedIn) {
      if (currentUser.user.is_tasker_data !== isTaskerData) {
        setIsTaskerData(true);
        requestTasker(currentUser.user.id);
      }
    }
    setInterval(async () => {
      if (authToken) {
        const isAuth = await isJwtTokenExpired(authToken);
        if (!isAuth) {
          localStorage.removeItem('AUTH_TOKEN');
          dispatch(logOut());
        }
      }
    }, 60000);
  }, [
    authToken,
    getUserByJwtToken,
    isJwtTokenExpired,
    dispatch,
    currentUser,
    requestTasker,
    currentTasker,
    getUserFollows,
    currentFollows,
    isTaskerData,
    follows,
    tasker,
  ]);

  const THEME = createTheme({
    typography: {
      fontFamily: `"Poppins", "Arial", sans-serif`,
      fontSize: 14,
      fontWeightLight: 300,
      fontWeightRegular: 400,
      fontWeightMedium: 500,
    },
  });

  return (
    <ThemeProvider theme={THEME}>
      <Header />
      <Container>
        <Suspense fallback={<LoadingPage />}>
          <Route />
        </Suspense>
      </Container>
    </ThemeProvider>
  );
};

export default App;
