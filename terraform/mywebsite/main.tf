module "mywebsite" {
  source      = "../s3-static-website"
  endpoint    = "mywebsite.dreamhomeplus.com"
  domain_name = "dreamhomeplus.com"
  region      = var.region
  bucket_name = "dreamhomeplus.com"
}