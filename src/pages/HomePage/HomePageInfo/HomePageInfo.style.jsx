import styled from 'styled-components';
import HomePageInfo from '../../../assets/homepage.jpg';
export const Box = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 500px;
  color: White;
  h1 {
    margin: 0px;
    font-size: 40px;
  }
  p {
    margin: 15px 0;
    font-size: 23px;
  }
  background: linear-gradient(to right bottom, rgba(68, 68, 68, 0.63), rgba(68, 68, 68, 0.63)), url(${HomePageInfo});
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  @media (max-width: 768px) {
    height: 600px;
  }
`;
export const Container = styled.div``;
export const PostTaskStyle = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  height: auto;
  padding: 0.3rem 1.6rem;
  color: White;
  font-size: 1rem;
  button {
    margin-top: 20px;
    margin-right: 30px;
  }
`;
