import React, { useEffect, useState, useMemo } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import _ from 'lodash';
import Box from '@mui/material/Box';
import DashboardMenu from '../DashboardItems/DashboardMenu';
import DashboardTitleLine from '../DashboardItems/DashboardTitleLine';
import DashboardTaskerForm from '../DashboardItems/DashboardTaskerForm';
import DashboardPostedTasks from '../DashboardItems/DashboardPostedTasks';
import DashboardUserProfile from '../DashboardItems/DashboardUserProfile';
import DashboardPayment from '../DashboardItems/DashboardPayment';
import DashboardTaskerProfile from '../DashboardItems/DashboardTaskerProfile';
import DashboardHome from '../DashboardItems/DashboardHome';
import DashboardTasker from '../DashboardItems/DashboardTasker';
import TaskerDiasbleInfo from '../DashboardItems/DashboadDisableInfo/TaskerDiasbleInfo';

const Dashboard = ({ tasksOfUser, tasksOfTasker }) => {
  const [title, setTitle] = useState('Home');
  const { user, tasker } = useSelector((state) => state.currentUser);
  const [isTasker, setIsTasker] = useState(false);
  const TaskerEditableItems = useMemo(() => ['Payment', 'Profile'], []);
  const userEditableItems = useMemo(() => ['User Profile'], []);
  const turnOnOffItem = useMemo(() => ['Tasker Dashboard'], []);
  const taskerItems = useMemo(() => ['Tasks', 'Payment', 'Profile'], []);

  const onMenuClick = (menuItem) => {
    setTitle(menuItem);
  };

  useEffect(() => {
    if (user.is_tasker !== isTasker) {
      setIsTasker(user.is_tasker);
    }
  }, [user, tasker, isTasker]);

  const dashboardTitle = useMemo(() => {
    if (userEditableItems.includes(title)) {
      return <DashboardTitleLine title={title} edit />;
    } else if (TaskerEditableItems.includes(title)) {
      return <DashboardTitleLine title={title} edit disable={!isTasker} />;
    } else if (turnOnOffItem.includes(title)) {
      return <DashboardTitleLine title={title} taskMode />;
    } else if (title) {
      return <DashboardTitleLine title={title} />;
    }
  }, [TaskerEditableItems, userEditableItems, title, turnOnOffItem, isTasker]);

  const TaskerDashboardStatus = useMemo(() => {
    if (!_.isEmpty(user)) {
      if (!user.is_tasker) {
        return <TaskerDiasbleInfo message="Not a tasker yet!" />;
      } else {
        setIsTasker(true);
        if (!user.is_tasker_data) {
          return <DashboardTaskerForm />;
        } else {
          return <DashboardTasker tasksOfTasker={tasksOfTasker} />;
        }
      }
    }
  }, [user, tasksOfTasker]);

  const taskerItemsPresent = useMemo(() => {
    if (isTasker && user.is_tasker_data) {
      switch (title) {
        case 'Tasks':
          return <DashboardPostedTasks tasksOfTasker={tasksOfTasker} />;
        case 'Payment':
          return <DashboardPayment />;
        case 'Profile':
          return <DashboardTaskerProfile />;
        default:
          return;
      }
    } else {
      if (taskerItems.includes(title)) {
        if (!isTasker) {
          return <TaskerDiasbleInfo message="Not a tasker yet!" />;
        }
        if (!user.is_tasker_data) {
          return <TaskerDiasbleInfo message="No tasker data yet!" />;
        }
      } else {
        return;
      }
    }
  }, [isTasker, taskerItems, title, tasksOfTasker, user]);

  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'row',
        width: '1152px',
        justifyContent: 'center',
        color: '#444',
        minHeight: '92vh',
        paddingTop: 2,
        bgcolor: 'background.paper',
      }}
    >
      <DashboardMenu onMenuClick={onMenuClick} />
      <Box sx={{ width: 852 }}>
        {dashboardTitle}
        <Box sx={{ width: '100%', paddingTop: '1.5rem', paddingLeft: '3.5rem' }}>
          {title === 'Home' && <DashboardHome tasksOfUser={{ ...tasksOfUser, ...tasksOfTasker }} />}
          {title === 'Posted Tasks' && <DashboardPostedTasks tasksOfUser={tasksOfUser} />}
          {title === 'Tasker Dashboard' && TaskerDashboardStatus}
          {title === 'User Profile' && <DashboardUserProfile />}
          {taskerItemsPresent}
        </Box>
      </Box>
    </Box>
  );
};

Dashboard.propTypes = {
  tasksOfUser: PropTypes.any,
  tasksOfTasker: PropTypes.any,
};

export default Dashboard;
