import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import Avatar from '@mui/material/Avatar';
import Stack from '@mui/material/Stack';
import stringAvatar from '../../utils/avatar.util';

const ImageAvatars = ({ link, user }) => (
  <Link to={`/${link}`}>
    <Stack direction="row" spacing={2}>
      <Avatar {...stringAvatar(user.name)} src={user.avatar} />
    </Stack>
  </Link>
);

ImageAvatars.propTypes = {
  link: PropTypes.string.isRequired,
  username: PropTypes.string.isRequired,
  user: PropTypes.object.isRequired,
};

export default ImageAvatars;
