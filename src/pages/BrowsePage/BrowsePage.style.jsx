import styled from 'styled-components';

export const Browse = styled.div`
  overflow: hidden;
  height: 94vh;
`;

export const ToolBar = styled.div`
  display: flex;
  width: 100%;
  justify-content: center;
  background-color: #fff;
  border-bottom: 1px solid #d4d2d5;
  .tools {
    display: flex;
    max-width: 1152px;
    height: 90px;
    align-items: center;
    justify-content: space-around;
    position: relative;
    top: 0.8rem;
    gap: 35px;
    .locationCondition {
      min-width: 230px;
      max-width: 300px;
    }
    .toolButtonsSet {
      display: flex;
      align-items: center;
    }
  }
  .MuiSelect-select {
    color: #0d9ddb;
  }
`;

export const Display = styled.div`
  display: flex;
  justify-content: center;
  position: relative;
  left: 50%;
  transform: translateX(-50%);
  height: 90vh;
  max-width: 1152px;
  .details {
    width: 780px;
    height: 86vh;
    padding-top: 0;
  }
`;

export const Contents = styled.div`
  display: flex;
  flex-wrap: wrap;
  height: 93vh;
  padding-top: 30px;
  justify-content: center;
  box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
  gap: 20px;
  width: 350px;
  overflow: scroll;
  background-color: #fff;
  .loadingAndError {
    width: 100%;
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
  }
`;

export const LocationBox = styled.div`
  width: 200px;
  height: 265px;
  background-color: #fff;
  box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
  border-radius: 5px;
  position: absolute;
  z-index: 99;
  top: 80%;
  margin-left: -1.3rem;
  display: flex;
  flex-direction: column;
  font-family: roboto;
  align-items: center;
  padding: 1rem;
  .MuiTextField-root {
    margin-top: 0.8rem;
  }
  .distanceBox {
    width: 75%;
    display: flex;
    align-items: center;
    label {
      text-align: center;
    }
    justify-content: space-between;
  }
  .locationBox-buttons {
    margin-top: 0.8rem;
    padding-left: 8px;
  }
`;
