import styled from 'styled-components';

export const Root = styled.div`
  margin-left: 5;
  margin-right: 5;

  @media (max-width: 768px) {
    margin-top: 10px;
  }
`;
