import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Avatar from '@mui/material/Avatar';
import Chip from '@mui/material/Chip';
import stringAvatar from '../../utils/avatar.util';
import { InfoCard } from './informationCard.style';

const circle = {
  width: '45px',
  height: '45px',
  top: '5px',
  position: 'relative',
  borderRadius: '50%',
  marginRight: '15px',
};

const InformationCard = ({ icon, title, content, avatar, isAvatar }) => (
  <InfoCard>
    {isAvatar && <Avatar {...stringAvatar(content)} alt={content} src={avatar} />}
    {icon && (
      <div className="iconBox">
        <Chip icon={icon} sx={circle} />
      </div>
    )}
    <div className="info">
      <h3>{title}</h3>
      <span className={classNames({ numbers: !icon && !isAvatar })}>{content}</span>
    </div>
  </InfoCard>
);

InformationCard.propTypes = {
  content: PropTypes.node.isRequired,
  icon: PropTypes.node,
  title: PropTypes.string.isRequired,
  avatar: PropTypes.string,
  isAvatar: PropTypes.bool,
};

export default InformationCard;
