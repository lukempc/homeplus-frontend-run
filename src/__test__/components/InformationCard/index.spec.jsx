import React from 'react';
import InformationCard from '../../../components/InformationCard';
import MapsHomeWorkIcon from '@mui/icons-material/MapsHomeWork';
import { shallow } from 'enzyme';

describe('../../../components/InformationCard', () => {
  it('should render InformationCard by default', () => {
    const wrapper = shallow(<InformationCard  title="Type" content={"sda"} icon={<MapsHomeWorkIcon />}/ >);
    expect(wrapper.containsAnyMatchingElements([
      <h3>Type</h3>
    ]));
  });
});