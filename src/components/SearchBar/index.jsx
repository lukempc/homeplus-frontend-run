import { React, useState } from 'react';
import PropTypes from 'prop-types';
import Paper from '@mui/material/Paper';
import InputBase from '@mui/material/InputBase';
import IconButton from '@mui/material/IconButton';
import SearchIcon from '@mui/icons-material/Search';

const SearchBar = ({ placeholder, width, submitSearch }) => {
  const [inputValue, setInputValue] = useState('');

  const onSubmit = (e) => {
    e.preventDefault();
    if (!inputValue || inputValue === '') return;
    submitSearch(inputValue);
  };
  return (
    <Paper
      component="form"
      sx={{
        p: '2px 5px',
        display: 'flex',
        alignItems: 'center',
        width: { width },
        borderRadius: '5px',
      }}
      onSubmit={onSubmit}
    >
      <InputBase
        sx={{ ml: 1, flex: 1 }}
        placeholder={placeholder}
        inputProps={{ 'aria-label': 'search bar' }}
        onChange={(e) => setInputValue(e.target.value)}
        value={inputValue}
      />
      <IconButton type="submit" sx={{ p: '10px' }} aria-label="search">
        <SearchIcon />
      </IconButton>
    </Paper>
  );
};

SearchBar.propTypes = {
  placeholder: PropTypes.string,
  width: PropTypes.string,
  submitSearch: PropTypes.func,
};

export default SearchBar;
