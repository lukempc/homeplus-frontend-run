import { React } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import TextField from '@mui/material/TextField';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import CustomDatePicker from '../../../FormItems/DatePicker';

import { updateField, backToInit } from '../../../../store/reducers/form/form.actions';
import TimeCard from './TimeChoiceCard';
import ToggleButtons from '../../../FormItems/ToggleButtons';

const TitleTime = ({ isTitleError, values, updateFields, formBackToInit }) => {
  const handleDateChange = (value) => {
    updateFields('date', value);
  };

  const handleCheckChange = () => {
    updateFields('certain_time_check', !values.certain_time_check);
    updateFields('certain_time', values.certain_time_check || 0);
  };

  const handleRadioChange = (num) => {
    updateFields('certain_time', num);
  };

  const handleToggleButtons = async (value) => {
    formBackToInit();
    updateFields('category', value);
  };

  const timeSections = [1, 2, 3, 4];

  return (
    <div>
      <h4>What kind of task do you need?</h4>
      <ToggleButtons handleToggleButtons={handleToggleButtons} defaultCategory={values.category} />
      <h4>In a few words, what do you need to be done?</h4>
      <TextField
        id="title"
        label="Task Title"
        variant="outlined"
        name="title"
        value={values.title}
        onChange={(e) => updateFields(e.target.name, e.target.value)}
        sx={{ width: 600 }}
        error={isTitleError && values.title.length < 10}
        helperText={isTitleError && values.title.length < 10 ? 'Must be at least 10 characters' : ' '}
      />
      <h4>When do you need this to be done?</h4>
      <CustomDatePicker label="Task Date" value={values.date} onChange={(value) => handleDateChange(value)} />
      <FormGroup>
        <FormControlLabel
          className="certain-time-check"
          control={<Checkbox onChange={handleCheckChange} checked={values.certain_time_check} />}
          label="I need a certain time of day"
        />
      </FormGroup>
      {values.certain_time_check &&
        timeSections.map((section, index) => (
          <FormControlLabel
            value={section}
            key={index}
            control={
              <TimeCard
                certainTime={section}
                selected={section === values.certain_time}
                onClick={() => handleRadioChange(section)}
              />
            }
            label=""
          />
        ))}
    </div>
  );
};

TitleTime.propTypes = {
  isTitleError: PropTypes.bool,
  values: PropTypes.object,
  updateFields: PropTypes.func,
  formBackToInit: PropTypes.func,
};

const mapDispatchToProps = (dispatch) => ({
  updateFields: (name, value) => dispatch(updateField(name, value)),
  formBackToInit: () => dispatch(backToInit()),
});

const mapStateToProps = ({ taskForm }) => ({
  values: taskForm,
});

export default connect(mapStateToProps, mapDispatchToProps)(TitleTime);
