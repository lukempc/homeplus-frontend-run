import styled from 'styled-components';

export const Container = styled.div`
  @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@100&display=swap');

  margin-top: 6vh;
  position: absolute;
  width: 100%;
  font-family: 'Poppins', sans-serif;
  left: 0;
  background-color: #f2f2f2;
  a {
    text-decoration: none;
  }
  a:-webkit-any-link {
    color: #444;
  }
`;
