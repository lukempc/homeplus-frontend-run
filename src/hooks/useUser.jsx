import { useState } from 'react';
import AppApi from '../utils/axios';

const UseUser = () => {
  const [isUserError, setError] = useState(false);
  const [isUserLoading, setLoading] = useState(false);
  const [userEmail, setUserEmail] = useState(null);
  const [user, setUser] = useState();

  const verifyEmailExists = async (email) => {
    const data = await AppApi('get', `/signup?email=${email}`);
    return data;
  };

  const register = async (values) => {
    const res = await AppApi('post', '/signup', values);
    return res;
  };

  const setTaskerMode = async (user_id) => {
    const res = await AppApi('put', `/on-off-tasker?id=${user_id}`);
    return res;
  };

  const setIsTaskerData = async (user_id) => {
    const res = await AppApi('put', `/is-tasker-data?id=${user_id}`);
    return res;
  };

  const updateUser = async (values) => {
    const res = await AppApi('put', `/update-user`, values);
    return res;
  };

  const accountVerify = async (token) => {
    const res = await AppApi('put', `/confirm?token=${token}`);
    return res;
  };

  const resendVerify = async (token) => {
    const res = await AppApi('put', `/resend?token=${token}`);
    return res;
  };

  const passwordResetMail = async (email) => {
    const res = await AppApi('get', `/send-reset-email?email=${email}`);
    return res;
  };

  const resetPassword = async (values) => {
    const res = await AppApi('put', `/reset-password`, values);
    return res;
  };

  const login = async (body) => {
    const res = await AppApi('get', `/login?email=${body.email}&password=${body.password}`);
    return res;
  };

  const getUserByJwtToken = async (jwtToken) => {
    const data = await AppApi('get', `/getUser?jwt=${jwtToken}`);
    return data;
  };

  const isJwtTokenExpired = async (jwtToken) => {
    const data = await AppApi('get', `/auth?jwt=${jwtToken}`);
    return data;
  };

  const getUserEmailByToken = async (token) => {
    const email = await AppApi('get', `/get-email-by-token?token=${token}`);
    setUserEmail(email);
  };

  const getUser = async (user_id) => {
    const data = await AppApi('get', `/user?id=${user_id}`);

    if (!data) {
      setError('Somthing went wrong');
      setLoading(false);
      return;
    }

    return data;
  };

  const requestUser = async (user_id) => {
    setLoading(true);
    setError(false);

    const data = await getUser(user_id);

    setLoading(false);
    setUser(data);
  };

  return {
    verifyEmailExists,
    getUserByJwtToken,
    requestUser,
    resendVerify,
    register,
    login,
    getUser,
    setTaskerMode,
    accountVerify,
    passwordResetMail,
    resetPassword,
    user,
    isUserError,
    isUserLoading,
    userEmail,
    updateUser,
    setIsTaskerData,
    isJwtTokenExpired,
    getUserEmailByToken,
  };
};

export default UseUser;
