import styled from 'styled-components';

export const DetailCard = styled.div`
  h2,
  h3,
  span {
    font-weight: 400;
  }
  overflow: scroll;
  width: 100%;
  height: 82vh;
  position: relative;
  background-color: #fff;
  left: 50%;
  padding-bottom: 30rem;
  transform: translateX(-50%);
  box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
  .longLine {
    width: 90%;
    border: 1px solid #f2f2f2;
    position: relative;
    left: 50%;
    transform: translateX(-50%);
  }
  > div {
    display: flex;
    align-items: center;
    justify-content: space-around;
    width: 100%;
    margin-top: 2%;
  }
  .taskStatus {
    width: 50%;
    justify-content: start;
    margin-top: 1.8rem;
    padding-left: 2rem;
  }
  .cardHeader {
    margin-top: -0.5rem;
    margin-bottom: 0.8rem;
    padding-left: 3%;
    width: 95%;
    .cardTitle {
      display: flex;
      align-items: center;
      justify-content: space-between;
      width: 60%;
      margin-top: 3%;
      > div {
        display: flex;
      }
    }
  }
  .cardImportInfo {
    display: inline-grid;
    grid-template-columns: auto auto auto;
    height: 75px;
  }
  .cardOtherInfo {
    justify-content: space-between;
    align-items: start;
    display: inline;
    > div {
      display: flex;
    }
    .taskInfo {
      display: flex;
      width: 90%;
      height: 28rem;
      margin-left: 30px;
      .taskSubInfo {
        margin-top: 3%;
        width: 15%;
        display: inline;
        > div {
          padding: 15%;
        }
      }
      .description {
        line-height: 1.6;
        width: 70%;
        padding: 3%;
      }
    }
    .offersBox {
      display: inline-block;
      padding: 5%;
    }
    form {
      margin-bottom: 25px;
    }
    .commentArea {
      display: inline-block;
      width: 90%;
      position: relative;
      justify-content: start;
      padding: 5%;
      .commentBox {
        width: 100%;
        position: relative;
        justify-content: space-around;
      }
    }
  }
`;

export const Container = styled.div`
  width: 100%;
  height: 82vh;
  position: absolute;
  justify-content: center;
  align-items: center;
  align-above: 1px;
  z-index: 9999;
`;
