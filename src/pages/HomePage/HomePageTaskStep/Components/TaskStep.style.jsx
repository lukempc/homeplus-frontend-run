import styled from 'styled-components';

export const Box = styled.div`
  display: flex;
  flex-direction: row;
  margin: 0 10px 10px;
  font-family: 'Poppins', sans-serif;
`;
export const StepStyle = styled.div`
  background: #0d9ddb;
  border-radius: 50%;
  min-width: 40px;
  height: 40px;
  margin-right: 15px;
`;
export const StepNum = styled.div`
  display: flex;
  text-align: center;
  justify-content: center;
  font-size: 20px;
  color: #ffffff;
  position: relative;
  margin-top: 7px;
`;
export const StepDescription = styled.div`
  font-family: 'Poppins', sans-serif;
  font-size: 18px;
  color: #444;
  justify-content: center;
  margin-top: 10px;
`;
