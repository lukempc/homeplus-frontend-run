import styled from 'styled-components';

export const Box = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
`;
export const Container = styled.div`
  background-color: #292b32;
  display: flex;
  flex-direction: row;
  justify-content: center;
  height: 450px;
`;
