import styled from 'styled-components';
export const Box = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-bottom: 20px;
  margin-top: 30px;
  @media (max-width: 768px) {
  }
`;

export const Container = styled.div`
  width: 100%;
  margin-bottom: 30px;
  justify-content: center;
`;
export const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  margin: auto;
  justify-content: center;

  @media (max-width: 768px) {
    max-width: 350px;
    flex-direction: column;
    flex-wrap: wrap;
    text-align: left;
  }
`;
export const CardContainer = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  max-width: 1000px;
  flex-wrap: wrap;
  justify-content: center;
  align-items: center;
`;
