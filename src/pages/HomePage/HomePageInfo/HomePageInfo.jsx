import React from 'react';
import CustomButton from '../../../components/CustomButton';
import { Box, Container, PostTaskStyle } from './HomePageInfo.style';

const HomePageInfo = () => (
  <Box>
    <PostTaskStyle>
      <h1>
        Connect with experts to get the job <br />
        done on HomePlus
      </h1>
      <p>It’s amazing to what you can’t do yourself</p>
      <Container>
        <CustomButton color="primary" variant="contained" size="large">
          Post your task for free
        </CustomButton>
        <CustomButton color="secondary" variant="contained" size="large">
          Become a Tasker
        </CustomButton>
      </Container>
    </PostTaskStyle>
  </Box>
);

export default HomePageInfo;
