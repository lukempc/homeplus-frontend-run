import React, { useState, useEffect, useCallback, useMemo } from 'react';
import { Link, useParams, useNavigate } from 'react-router-dom';
import _ from 'lodash';
import { useSelector } from 'react-redux';
import FormControl from '@mui/material/FormControl';
import TextField from '@mui/material/TextField';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import MapIcon from '@mui/icons-material/Map';
import TaskCard from '../../components/TaskCard';
import CustomButton from '../../components/CustomButton';
import useTaskForm from '../../hooks/useTaskForm';
import SearchBar from '../../components/SearchBar';
import LoadingPage from '../../components/LoadingPage';
import { sortTypeSwitch } from './utils/SelectionItems';
import Map from '../../components/Map';
import { Display, Browse, ToolBar, Contents, LocationBox } from './BrowsePage.style';
import TaskDetailCard from '../../components/TaskDetailCard';
import useComment from '../../hooks/useComment';
import useOffer from '../../hooks/useOffer';

const BrowsePage = () => {
  const navigate = useNavigate();
  const { task_id } = useParams();
  const currentUser = useSelector((state) => state.currentUser);
  const { submitRequest, getTaskById, isError, isLoading, tasks, taskById } = useTaskForm();
  const { commentsRequest, comments } = useComment();
  const { offersRequest, offers } = useOffer();
  const [type, setType] = useState('a');
  const [sortType, setSortType] = useState('recent');
  const [suburb, setSuburb] = useState('');
  const [distance, setDistance] = useState('');
  const [isSearch, setSearch] = useState(false);
  const [locationBox, setLocationBox] = useState(false);
  const [postcode, setPostcode] = useState('');
  const [state, setState] = useState('');
  const [isTypeError, setTypeError] = useState(false);
  const [location, setLocation] = useState('Set location coditions');
  const [searchKeyword, setSearchKeyword] = useState(' ');
  const [originTask, setOriginTask] = useState(null);
  const [displayTask, setTask] = useState(null);
  const [taskId, setTaskId] = useState(null);
  const [isResetTask, setRestTask] = useState(false);

  const backToBrowser = useCallback(() => navigate('/browse-tasks', { replace: true }), [navigate]);

  const address = useMemo(() => {
    return [suburb, postcode, state].filter(Boolean).join(', ');
  }, [suburb, postcode, state]);

  const toggleLocationBox = () => {
    setLocationBox(!locationBox);
  };

  useEffect(() => {
    if (isResetTask) {
      commentsRequest(displayTask.id);
      offersRequest(displayTask.id);

      setRestTask(false);
    }
    if (taskId !== task_id && task_id) {
      setTaskId(task_id);
      getTaskById(task_id);
      commentsRequest(task_id);
      offersRequest(task_id);
    }
    if (!task_id && taskId !== task_id) {
      setTaskId(null);
      getTaskById(null);
    }
    if (originTask !== taskById) {
      setOriginTask(taskById);
      setTask(() => {
        if (taskById) {
          if (currentUser.followedTasks?.map((follow) => follow.id).includes(taskById.id)) {
            return { ...taskById, following: true };
          } else {
            return { ...taskById, following: false };
          }
        } else {
          return taskById;
        }
      });
    }
  }, [
    commentsRequest,
    offersRequest,
    displayTask,
    isResetTask,
    task_id,
    taskId,
    getTaskById,
    taskById,
    currentUser,
    originTask,
  ]);

  const chooseTask = useCallback((task) => {
    setTask(task);
    setRestTask(true);
  }, []);

  const applyConditions = () => {
    setTypeError(false);
    if (suburb + postcode + state + distance === '') {
      setLocation('Set location coditions');
      setLocationBox(!locationBox);
    } else if (suburb && postcode && state && distance) {
      setLocation([suburb, postcode, state, distance + ' km'].filter(Boolean).join(', '));
      setLocationBox(!locationBox);
      submitRequest(address, parseInt(distance) * 1000, searchKeyword);
    } else {
      setTypeError(true);
    }
    backToBrowser();
  };

  const clearConditions = () => {
    setLocation('Set location coditions');
    setPostcode('');
    setState('');
    setSuburb('');
    setDistance('');
    setTypeError(false);
    submitRequest(' ', 0, ' ');
    setSearch(false);
    backToBrowser();
  };

  const handleTypeChange = (event) => {
    setType(event.target.value);
  };

  const handleSortTypeChange = (event) => {
    setSortType(event.target.value);
  };

  const submitSearch = (keyword) => {
    setSearchKeyword(keyword);
    setSearch(true);
    submitRequest(address(), parseInt(distance * 1000), keyword);
    backToBrowser();
  };

  const handleReset = () => {
    clearConditions();
    setType('a');
    setSortType('recent');
    setSearchKeyword(' ');
    submitRequest(' ', 0, ' ');
    setSearch(false);
    backToBrowser();
  };

  const tasksToDisplay = useCallback(
    (displayTasks) => {
      if (tasks) {
        let filteredTasks = Object.values(displayTasks)
          ?.filter((task) => task.category.includes(type))
          .sort(sortTypeSwitch(sortType));
        if (!_.isEmpty(currentUser.followedTasks)) {
          const followingTasks = currentUser.followedTasks.map((follow) => follow.id);
          filteredTasks = filteredTasks.map((task) => {
            return { ...task, following: followingTasks.includes(task.id) };
          });
        }
        return filteredTasks;
      } else {
        return [];
      }
    },
    [type, sortType, tasks, currentUser]
  );

  const presentTasks = useMemo(() => {
    if (!isError && !!tasksToDisplay(tasks).length) {
      return tasksToDisplay(tasks).map((task) => (
        <Link to={`/browse-tasks/${task.id}/${task.title}`} key={task.id}>
          <TaskCard details={task} onClick={() => chooseTask(task)} actived={task.id === taskId} />
        </Link>
      ));
    } else if (!isError && !tasksToDisplay(tasks).length) {
      return <span>No results</span>;
    } else {
      return <span>{isError}</span>;
    }
  }, [isError, tasks, taskId, tasksToDisplay, chooseTask]);

  return (
    <Browse>
      <ToolBar>
        <div className="tools">
          <SearchBar placeholder="keywords" width="500" submitSearch={submitSearch} />
          <div className="locationCondition">
            <CustomButton onClick={toggleLocationBox}>{location}</CustomButton>
            {locationBox ? (
              <LocationBox>
                <TextField
                  id="filled-basic"
                  name="postcode"
                  value={postcode}
                  label="Postcode"
                  size="small"
                  sx={{ width: '150px' }}
                  onChange={(e) => setPostcode(e.target.value)}
                  error={isTypeError}
                />
                <TextField
                  id="filled-basic"
                  name="suburb"
                  value={suburb}
                  label="Suburb"
                  size="small"
                  sx={{ width: '150px' }}
                  onChange={(e) => setSuburb(e.target.value)}
                  error={isTypeError}
                />
                <TextField
                  id="filled-basic"
                  name="state"
                  value={state}
                  label="State"
                  size="small"
                  sx={{ width: '150px' }}
                  onChange={(e) => setState(e.target.value)}
                  error={isTypeError}
                />
                <div className="distanceBox">
                  <TextField
                    id="filled-basic"
                    name="ditance"
                    value={distance}
                    size="small"
                    type="number"
                    sx={{ width: '100px' }}
                    onChange={(e) => (e.target.value < 0 ? setDistance(0) : setDistance(e.target.value))}
                    error={isTypeError}
                  />
                  <span>km</span>
                </div>
                <div className="locationBox-buttons">
                  <CustomButton color="primary" variant="outlined" size="small" onClick={clearConditions}>
                    Clear
                  </CustomButton>
                  <CustomButton color="primary" variant="contained" size="small" onClick={applyConditions}>
                    Apply
                  </CustomButton>
                </div>
              </LocationBox>
            ) : null}
          </div>
          <div>
            <FormControl variant="standard" sx={{ m: 1, minWidth: 150, fontFamily: "'Poppins', sans-serif" }}>
              <Select
                id="type-select"
                value={type}
                onChange={handleTypeChange}
                sx={{ fontFamily: "'Poppins', sans-serif" }}
              >
                <MenuItem value={'a'}>All</MenuItem>
                <MenuItem value={'cleaning'}>Cleaning only</MenuItem>
                <MenuItem value={'removal'}>Moving only</MenuItem>
                <MenuItem value={'handyperson'}>Repair only</MenuItem>
              </Select>
            </FormControl>
          </div>
          <div>
            <FormControl variant="standard" sx={{ m: 1, minWidth: 150, color: '#fff' }}>
              <Select id="sort-select" value={sortType} onChange={handleSortTypeChange}>
                <MenuItem value={'recent'}>Date: Most Recent</MenuItem>
                <MenuItem value={'future'}>Date: Futrue</MenuItem>
                <MenuItem value={'high budget'}>Budget: High - Low</MenuItem>
                <MenuItem value={'low budget'}>Budget: Low - High</MenuItem>
              </Select>
            </FormControl>
          </div>
          <div className="toolButtonsSet">
            <CustomButton color="primary" size="small" variant="outlined" onClick={handleReset}>
              Reset
            </CustomButton>
            <CustomButton onClick={backToBrowser}>
              <MapIcon sx={{ fontSize: 40 }} />
            </CustomButton>
          </div>
        </div>
      </ToolBar>
      {isLoading && <LoadingPage />}
      {!isLoading && (
        <Display>
          <Contents>
            {tasks && presentTasks}
            <div className="loadingAndError">{isError && <span>{isError}</span>}</div>
          </Contents>
          <div className="details">
            {displayTask && (
              <TaskDetailCard
                details={displayTask}
                setTask={chooseTask}
                taskComments={comments ? comments : []}
                taskOffers={offers ? offers : []}
              />
            )}
            <Map tasks={tasks ? tasksToDisplay(tasks) : []} address={address} distance={distance} isSearch={isSearch} />
          </div>
        </Display>
      )}
    </Browse>
  );
};

export default BrowsePage;
