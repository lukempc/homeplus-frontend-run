import React, { useMemo } from 'react';
import { useSelector } from 'react-redux';
import { Wrapper, Left, PicArea, Username, LineWrapper, Labal, Value } from './DashboardUserProfile.style';
import AvatarUploader from '../../AvatarUploader';

const DashboardUserProfile = () => {
  const { user } = useSelector((state) => state.currentUser);

  const address = useMemo(() => {
    if (user.postcode) {
      return `${user.street}, ${user.suburb}, ${user.state}, ${user.postcode}`;
    } else {
      return 'NaN';
    }
  }, [user]);

  return (
    <>
      <Wrapper>
        <Left>
          <PicArea>
            <AvatarUploader />
            <Username>{user.name}</Username>
          </PicArea>
          <div>
            <LineWrapper>
              <Labal>Gender</Labal>
              <Value>{user.gender ? user.gender : 'NaN'}</Value>
            </LineWrapper>
            <LineWrapper>
              <Labal>DOB</Labal>
              <Value>{user.date_of_birth ? user.date_of_birth : 'NaN'}</Value>
            </LineWrapper>
            <LineWrapper>
              <Labal>Email</Labal>
              <Value>{user.email}</Value>
            </LineWrapper>
            <LineWrapper>
              <Labal>Address</Labal>
              <Value>{address}</Value>
            </LineWrapper>
            <LineWrapper>
              <Labal>Mobile</Labal>
              <Value>{user.mobile ? user.mobile : 'NaN'}</Value>
            </LineWrapper>
          </div>
        </Left>
      </Wrapper>
    </>
  );
};

export default DashboardUserProfile;
