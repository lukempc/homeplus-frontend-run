import React from 'react';
import PropTypes from 'prop-types';
import { styled } from '@mui/system';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Button from '@mui/material/Button';

export const AlertDot = styled('div')({
  backgroundColor: '#e53e3e',
  padding: 6,
  borderRadius: 50,
  position: 'absolute',
  right: -5,
  top: -5,
});
AlertDot.displayName = 'AlertDot';

const theme = createTheme({
  palette: {
    primary: {
      main: '#0d9ddb',
      contrastText: '#fff',
      darker: '#166a8f',
    },
    secondary: {
      main: '#fff',
      contrastText: '#0d9ddb',
      darker: '#166a8f',
    },
    black: {
      main: '#444444',
      contrastText: '#fff',
    },
  },
});

const CustomButton = ({ children, alertDot, ...otherProps }) => (
  <ThemeProvider theme={theme}>
    <Button
      {...otherProps}
      sx={{
        mt: 1,
        mr: 1,
        textDecoration: 'none',
        textTransform: 'none',
        fontSize: 15,
        fontFamily: "'Poppins', sans-serif",
      }}
    >
      {alertDot ? <AlertDot /> : null}
      {children}
    </Button>
  </ThemeProvider>
);

CustomButton.propTypes = {
  children: PropTypes.node.isRequired,
  alertDot: PropTypes.bool,
};

export default CustomButton;
