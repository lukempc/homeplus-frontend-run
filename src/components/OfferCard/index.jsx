import React, { useCallback, useMemo, useState } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import ImageAvatars from '../Profile-piconclick';
import CustomButton from '../CustomButton';
import { Card, TitleRow, TaskerInfo, TaskerAva, ButtonSet, PosterOnly, OfferContent } from './OfferCard.style';
import AcceptRejectOfferModal from '../AcceptRejectOfferModal';

const OfferCard = ({ offer, isAssigned, setIsAssigned, setTask, taskDetails }) => {
  const currentUser = useSelector((state) => state.currentUser);
  const [showAcceptRejectOfferForm, setShowAcceptRejectOfferForm] = useState(false);
  const [decision, setDecision] = useState(offer.offer_status);
  const [displayOffer, setDisplayOffer] = useState(offer);
  const tasker = displayOffer.taskerEntity;

  const date = useMemo(() => {
    return `${displayOffer.created_time.split('T')[0].replaceAll('-', '/')}`;
  }, [displayOffer]);

  const changeOfferStatus = useCallback((status) => {
    setDecision(status);
    setShowAcceptRejectOfferForm(true);
  }, []);

  const acceptRejectButtonSet = useCallback(
    (status) => {
      return (
        <>
          <CustomButton
            size="small"
            color="primary"
            disabled={status}
            variant="contained"
            onClick={() => changeOfferStatus('ACCEPTED')}
          >
            Accept
          </CustomButton>
          <CustomButton size="small" color="black" disabled={status} onClick={() => changeOfferStatus('REJECTED')}>
            Reject
          </CustomButton>
        </>
      );
    },
    [changeOfferStatus]
  );

  const acceptedCancelButton = useCallback(
    (status) => {
      return (
        <CustomButton size="small" variant="contained" disabled={status} onClick={() => changeOfferStatus('CANCELED')}>
          Cancel
        </CustomButton>
      );
    },
    [changeOfferStatus]
  );

  const Buttons = useMemo(() => {
    switch (displayOffer.offer_status) {
      case 'ACCEPTED':
        return acceptedCancelButton(false || taskDetails.task_status === 'completed');
      case 'PENDING':
        return acceptRejectButtonSet(false || isAssigned);
      case 'REJECTED':
        return acceptRejectButtonSet(true);
      case 'CANCELED':
        return acceptedCancelButton(true);
      default:
        return;
    }
  }, [displayOffer, acceptedCancelButton, acceptRejectButtonSet, isAssigned, taskDetails]);

  return (
    <>
      <Card>
        <TitleRow>
          <TaskerInfo>
            <TaskerAva>
              <ImageAvatars user={tasker.userEntity} link={`tasker-detail/${tasker.id}/${tasker.userEntity.name}`} />
              <p>{tasker.userEntity.name}</p>
            </TaskerAva>
            <p>{date}</p>
          </TaskerInfo>
          {currentUser.user.id === displayOffer.taskEntity.userEntity.id && (
            <PosterOnly>
              <p>${displayOffer.offered_price}</p>
              <ButtonSet>{Buttons}</ButtonSet>
            </PosterOnly>
          )}
        </TitleRow>
        <hr />
        <OfferContent>{displayOffer.description}</OfferContent>
      </Card>
      <AcceptRejectOfferModal
        status={decision}
        offer={displayOffer}
        showModal={showAcceptRejectOfferForm}
        setShowModal={setShowAcceptRejectOfferForm}
        setDisplayOffer={setDisplayOffer}
        setIsAssigned={setIsAssigned}
        setTask={setTask}
        taskDetails={taskDetails}
      />
    </>
  );
};

OfferCard.propTypes = {
  offer: PropTypes.object.isRequired,
  setIsAssigned: PropTypes.func.isRequired,
  isAssigned: PropTypes.bool.isRequired,
  setTask: PropTypes.func.isRequired,
  taskDetails: PropTypes.object.isRequired,
};

export default OfferCard;
