import React from 'react';
import Link from '@mui/material/Link';
import { Box } from './Discover.style.jsx';

const Discover = () => (
  <Box>
    <h3>Discover</h3>
    <Link href="#">How it works</Link>
    <Link href="#">HomePlus for business</Link>
    <Link href="#">Earn money</Link>
    <Link href="#">Search jobs</Link>
    <Link href="#">Cost Guides</Link>
    <Link href="#">Service Guides</Link>
    <Link href="#">New users FAQ</Link>
  </Box>
);
export default Discover;
