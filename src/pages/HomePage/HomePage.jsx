import React from 'react';
import HomePageInfo from './HomePageInfo';
import HomePageTaskStep from './HomePageTaskStep';
import HomePageInstruction from './HomePageInstruction';
import Footer from '../../components/Footer';
import { Wrapper } from './HomePage.style';

const HomePage = () => (
  <Wrapper>
    <HomePageInfo />
    <HomePageTaskStep />
    <HomePageInstruction />
    <Footer />
  </Wrapper>
);

export default HomePage;
